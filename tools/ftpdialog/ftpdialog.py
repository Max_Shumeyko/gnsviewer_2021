# -*- coding: utf-8 -*-
"""
Created on Thu Apr  5 12:43:31 2018

FtpDialog provides typical QDialog service to choose and extract filename from outer host by FTP.
You can treat it in this way:

local_pathname = FtpDialog(parent):
    parent:  parent component to which the dialog can be bound.
    
Then you can connect to outer host by ftp and choose file to download using handy Commander-like interface.

Return:
    local_pathname: contains full local path of outer file downloaded to script folder (or script folder from which the dialog was started).
                    or None in case if the dialog was escaped by cancel button.
See ftp_client on https://github.com/jacklam718/ftp for details.
@author: eastwood
"""
import os
import sys
from threading import Thread
from ftplib import FTP
from PyQt5 import QtGui, QtCore

from .ftp_master.utils import fileProperty
from .ftp_master.dialog import loginDialog, ProgressDialog, DownloadProgressWidget, UploadProgressWidget
from .ftp_master import ftp_client as fc

class FTPDialogWidget(QtGui.QDialog,fc.FtpClient):
    
    def __init__(self,parent=None):
        QtGui.QDialog.__init__(self,parent)
        self.ftp = FTP()
        self.setupGui()
        self.downloads = []

        self.remote.homeButton.clicked.connect(self.cdToRemoteHomeDirectory)
        self.remote.fileList.itemDoubleClicked.connect(self.cdToRemoteDirectory)
        self.remote.backButton.clicked.connect(self.cdToRemoteBackDirectory)
        self.remote.nextButton.clicked.connect(self.cdToRemoteNextDirectory)
        self.remote.connectButton.clicked.connect(self.connect)
        # QtCore.QObject.connect(self.remote.pathEdit, QtCore.SIGNAL('returnPressed( )'), self.cdToRemotePath)
        self.remote.pathEdit.returnPressed.connect(self.cdToRemotePath)

        self.progressDialog = ProgressDialog(self)
        self.filePath = ''
        
        self.setWindowModality(QtCore.Qt.WindowModal)
        #self.show()
        #self.exec_()

    def setupGui(self):
        self.resize(740, 650)
        self.remote  = fc.LocalGuiWidget(self)
        mainLayout = QtGui.QHBoxLayout( )
        mainLayout.addWidget(self.remote)
        for pos, width in enumerate((150, 140, 70, 70, 150, 90)):
            self.remote.fileList.setColumnWidth(pos, width)
        self.remote.uploadButton.setVisible(False)
        self.setLayout(mainLayout)
        
    def cdToRemoteDirectory(self, item, column):
        pathname = os.path.join(self.pwd, str(item.text(0)))
        if not self.isRemoteDir(pathname):    
            self.filePath = pathname
            self.local_filePath = self.download()
            self.accept()
        else:
            super(FTPDialogWidget,self).cdToRemoteDirectory(item, column)
            
    def loadToLocaFileList(self):
        pass # to prevent araising AttributeError according to .local doesn't exist
   
    def download(self, local_filePath=None, offset=0):
        if local_filePath is None:
            local_filePath = os.path.join(os.getcwd(), os.path.basename(self.filePath))
        item = self.remote.fileList.currentItem( )
        filesize = int(item.text(1))
        
        pb = self.progressDialog.addProgress(
            type='download',
            title=self.filePath,
            size=filesize,
        )

        def callback(data):
            pb.set_value(data)
            file.write(data)

        file = open(local_filePath, 'wb')
        fp = FTP( )
        fp.connect(host=self.ftp.host, port=self.ftp.port, timeout=self.ftp.timeout)
        fp.login(user=self.ftp.user, passwd=self.ftp.passwd)
        fp.retrbinary(cmd='RETR '+self.filePath, callback=callback, rest=offset)
        return local_filePath
   
def FtpDialog(parent=None):
    '''
    FtpDialog provides typical QDialog service to choose and extract filename from outer host by FTP.
    You can treat it in this way:
    
    local_pathname = FtpDialog(parent):
        parent:  parent component to which the dialog can be bound.
        
    Then you can connect to outer host by ftp and choose file to download using handy Commander-like interface.
    
    Return:
        local_pathname: contains full local path of outer file downloaded to script folder (or script folder from which the dialog was started).
                        or None in case if the dialog was escaped by cancel button.
    See ftp_client on https://github.com/jacklam718/ftp for details.
    '''
    dialog = FTPDialogWidget(parent=parent)
    result = dialog.exec_( )
    if result:
        return dialog
    else:
        print('FtpDialog: button Cancel or Escaped was pressed', result)
        return None
        

if __name__ == '__main__':
### Example 1 FtpDialog     
    class Window(QtGui.QWidget):    
        def __init__(self, parent=None):
            super(Window,self).__init__(parent)
            self.makeGui()
            self.ftp_dialog = None
        
        def makeGui(self):
            self.setWindowTitle('FtpDialog Example')
            self.resize(300,150)
            button = QtGui.QPushButton('Show dialog window')
            button.clicked.connect(self.on_clicked)
            
            box = QtGui.QVBoxLayout()
            box.addWidget(button)
            self.setLayout(box)
            
        def on_clicked(self):
            self.ftp_dialog = FtpDialog(parent=self)
            if self.ftp_dialog is not None:
                print('FilePath (on host): ', self.ftp_dialog.filePath)
                print('FilePath (local): ', self.ftp_dialog.local_filePath)
            
    app = QtGui.QApplication(sys.argv)
    window = Window()
    window.show()
    
    #after working with the app, you can still download the file again - for example if it changes
    import time
    time.sleep(5)
    if window.ftp_dialog is not None:
        window.ftp_dialog.download()
        #use offset to skip offset bytes from the beginning of the file
        window.ftp_dialog.download(offset = 100)
        print('The file was downloaded again')
        
    sys.exit(app.exec_())

### Example 2 FTPDialogWidget
#    def on_clicked():
#        dialog = FTPDialogWidget(parent=window)
#        result = dialog.exec_()
#        print 'result: ',result
#        if result == QtGui.QDialog.Accepted:
#            print 'FilePath: ', dialog.filePath
#        else:
#            print 'Button Cancel or Escaped was pressed', result
#
#    app = QtGui.QApplication(sys.argv)
#    window = QtGui.QWidget()
#    window.setWindowTitle("Probe")
#    window.resize(300, 150)
#    button = QtGui.QPushButton("Show dialog window")
#    button.clicked.connect(on_clicked)
#    
#    box = QtGui.QVBoxLayout()
#    box.addWidget(button)
#    window.setLayout(box)
#    
#    window.show()
#    sys.exit(app.exec_())
