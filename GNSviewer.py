# -*- coding: utf-8 -*-
"""
Created on Wed Aug 23 12:03:03 2017
@author: eastwood

Vizualize data spectrums and distributions 
from detector box of Dubna Gas Filled Recoil Separator-2 (DGFRS-2).
Observe energy, channel and position distributions on 
detectors offline and online while the setup is running.

How to use:
1. Run the code.
2. Menu -> Open calibration folder
3. Menu -> Open/Open FTP to run the file.
4. Observe different distribution using clicks and side panel.

Data:
One can use it to read gns. tns. or .bin file where experiment data is 
collected.
-
Detectors contain:
    48 front strips
    128 back strips
    48 side strips
Scale:
    alpha [0 - 25 MeV]    
    fission [25 - 250 MeV]
Tof mark: (if the particle passes MWPC it is meant to come from the target,
if not - emmited alpha particle from detector strips)
    True
    False
    'all' (str) contains all events
-

Main window contains plots:
0   'Front Alpha'       [channel/energy, counts] distribution
1   'Front Fission'     [channel/energy, counts] distribution
2   'Back Alpha'        [channel/energy, counts] distribution   
3   'Back Fission'      [channel/energy, counts] distribution
4   'Side Alpha'        [channel/energy, counts] distribution
5   'Side Fission'      [channel/energy, counts] distribution
6   'Front position'    [counts, strip] distribution
7   'Back position'     [counts, strip] distribution
8   'dE1 - dE2'               [channel, counts] distribution
9   'Rotation Time'     [channel, counts] distribution
10  'Intensivity timeserie' [time, counts] distribution + chains     
11  -                   -
12  '3D position'       [front strip, back strip, counts] distribution
        
Project structure:

|MainWindow| // main widget
---class----|
            |--> |Interface|
                 ----------| --> |Menu|   <--------------------------|ReadDataTread| (!)// timer & spectrum funcs 
                           |     -----|--> Read data files           |ReaderClass| // raw processing functions
                           |          |--> Read data files using ftp  ----classes--
                           |          |--> Read calibration files
                           |
                           | --> |Layouts| <----------|PrimaryWidget| // 4 x 4 screens main window
                                  ^      | <----------|BoxWidget| // 4 x 4 screens same data graphs window
                                  |      | <----------|SideWidget| // Side Control buttons panel
                                  |      | <----------|singlewidget| // one big plot 
|VisualizeManager|-----------------                                 
-----class-------|  // data structures, plotting and updating data logics including buttons triggers                                      
                 | * initialize_layer_dict() -- > !lr_dict layer dictionary, 
                 |     contains all ierarchy of widgets and methods
                 | * Update() // method for plotting/updating the gui
                 | * parameters // all neccesary parameters for current screen (pg.parametertree)
                 | * plot_Main() // set data to widgets
"""                         
import sys
import os
import queue
import gc
import re

from datetime import datetime
from operator import attrgetter

import pandas as pd
import numpy as np

from scipy.optimize import curve_fit
from PyQt5 import QtGui
from PyQt5 import QtCore
from PyQt5.QtWidgets import (qApp, QWidget, QTextEdit, QInputDialog, QDialog,
    QLabel, QCheckBox, QLineEdit, QMessageBox)

import pyqtgraph as pg
import pyqtgraph.opengl as gl

from tools import read_digital as read_binary
# from tools import read_file_2020_march as read_analog
from tools import read_file_2020_dec as read_analog
from tools import spectrum as sp
from tools.ftpdialog.ftpdialog import FtpDialog
from tools.sftpdialog.sftpdialog import sFtpDialog
from tools.search_chain_AmCa import search_chain

check_a = None
check_b = None


class record:
    pass


def cur_time():
    return str(datetime.now())

def get_latest_file(files):
        """Return latest created file from the files list"""
        if files:
            return sorted(files, 
                          key=lambda file_: os.stat(file_).st_ctime)[-1]
        return False

def prepare_x(x):
    """Convert graphic x-axis format to histogram x-axis format.
    Graphic format: each point (x, y), len(X) == len(Y).
    Histogramm format: each bin([x_start, x_stop], y], len(X) == len(Y) + 1.

    """
    dx = x[1] - x[0]
    x = np.linspace(x[0] - dx, x[-1] + dx, len(x) + 1)
    return x

### Interface windows section ---
class SearchChainDialog(QDialog):
    
    def __init__(self, parent=None, 
                 search_properties=None, energy_bundle=None):
        super().__init__(parent)
        self.search_properties = search_properties
        self.energy_bundle = energy_bundle
        
        self.setWindowTitle("Set chain search parameters")
        self.make_interface()

    def make_interface(self):
        grid = QtGui.QGridLayout()
        
        # dt, min_num, max_num lineEdits
        self.lbl_dt = QLabel("Time window between two events (mcs): ")
        self.line_dt = QLineEdit()
        dt_IntValidator = QtGui.QIntValidator(1, 10 ** 12)
        self.line_dt.setValidator(dt_IntValidator)
        if self.search_properties is None:
            self.line_dt.setText('45000000')
        else:
            self.line_dt.setText(str(self.search_properties.time_dlt))
        
        self.lbl_min_ev = QLabel("Mininimum events in the chain: ")
        self.line_min_ev = QLineEdit()
        min_ev_IntValidator = QtGui.QIntValidator(3, 10)
        self.line_min_ev.setValidator(min_ev_IntValidator)
        if self.search_properties is None:
            self.line_min_ev.setText("3")
        else:
            self.line_min_ev.setText(
                str(self.search_properties.chain_length_min)
            )
        
        self.lbl_max_ev = QLabel("Maximum events in the chain: ")
        self.line_max_ev = QLineEdit()
        max_ev_IntValidator = QtGui.QIntValidator(4, 30)
        self.line_max_ev.setValidator(max_ev_IntValidator)
        if self.search_properties is None:
            self.line_max_ev.setText("15")
        else:
            self.line_max_ev.setText(
                str(self.search_properties.chain_length_max)
            )
             
        # Check boxes
        check_dict = {False: QtCore.Qt.Unchecked,
                      True: QtCore.Qt.Checked}    
            
        self.lbl_sr = QLabel("First event in the chains is recoil: ")
        self.check_sr = QCheckBox()
        if self.search_properties is None:
            self.check_sr.setCheckState(QtCore.Qt.Checked)
        else:
            self.check_sr.setCheckState(
                check_dict[self.search_properties.recoil_first]
            )
        
        self.lbl_ss = QLabel("Include side events: ")
        self.check_ss = QCheckBox()
        if self.search_properties is None:
            self.check_ss.setCheckState(QtCore.Qt.Checked)
        else:
            self.check_ss.setCheckState(
                check_dict[self.search_properties.search_side]
            )
        
        self.lbl_sf = QLabel("Include fission events: ")
        self.check_sf = QCheckBox()
        if self.search_properties is None:
            self.check_sf.setCheckState(QtCore.Qt.Checked)
        else:
            self.check_sf.setCheckState(
                check_dict[self.search_properties.search_fission]
            )
        
        # set energy limits       
        self.lbl_r_en = QLabel("Recoil energies min - max (KeV): ")
        self.line_r_en_min = QLineEdit()
        r_en_min_IntValidator = QtGui.QIntValidator(500, 15000)
        self.line_r_en_min.setValidator(r_en_min_IntValidator)
        if self.energy_bundle is None:
            self.line_r_en_min.setText("4000")
        else:
            self.line_r_en_min.setText(str(self.energy_bundle[0][0]))
        #
        self.line_r_en_max = QLineEdit()
        r_en_max_IntValidator = QtGui.QIntValidator(500, 25000)
        self.line_r_en_max.setValidator(r_en_max_IntValidator)
        if self.energy_bundle is None:
            self.line_r_en_max.setText("28000")
        else:
            self.line_r_en_max.setText(str(self.energy_bundle[0][1]))
        
        self.lbl_a_en = QLabel("Alpha energies min - max (KeV): ")
        self.line_a_en_min = QLineEdit()
        a_en_min_IntValidator = QtGui.QIntValidator(500, 15000)
        self.line_a_en_min.setValidator(a_en_min_IntValidator)
        if self.energy_bundle is None:
            self.line_a_en_min.setText("7800")
        else:
            self.line_a_en_min.setText(str(self.energy_bundle[1][0]))
        #
        self.line_a_en_max = QLineEdit()
        a_en_max_IntValidator = QtGui.QIntValidator(500, 25000)
        self.line_a_en_max.setValidator(a_en_max_IntValidator)
        if self.energy_bundle is None:
            self.line_a_en_max.setText("15000")
        else:
            self.line_a_en_max.setText(str(self.energy_bundle[1][1]))
        
        self.lbl_s_en = QLabel("Side alpha energies min - max (KeV): ")
        self.line_s_en_min = QLineEdit()
        s_en_min_IntValidator = QtGui.QIntValidator(500, 15000)
        self.line_s_en_min.setValidator(s_en_min_IntValidator)
        if self.energy_bundle is None:
            self.line_s_en_min.setText("7800")
        else:
            self.line_s_en_min.setText(str(self.energy_bundle[2][0]))
        #
        self.line_s_en_max = QLineEdit()
        s_en_max_IntValidator = QtGui.QIntValidator(500, 25000)
        self.line_s_en_max.setValidator(s_en_max_IntValidator)
        if self.energy_bundle is None:
            self.line_s_en_max.setText("13000")
        else:
            self.line_s_en_max.setText(str(self.energy_bundle[2][1]))
        
        self.lbl_f_en = QLabel("Fission energies min - max (KeV): ")
        self.line_f_en_min = QLineEdit()
        f_en_min_IntValidator = QtGui.QIntValidator(25000, 150000)
        self.line_f_en_min.setValidator(f_en_min_IntValidator)
        if self.energy_bundle is None:
            self.line_f_en_min.setText("40000")
        else:
            self.line_f_en_min.setText(str(self.energy_bundle[3][0]))
        #
        self.line_f_en_max = QLineEdit()
        f_en_max_IntValidator = QtGui.QIntValidator(25000, 250000)
        self.line_f_en_max.setValidator(f_en_max_IntValidator)
        if self.energy_bundle is None:
            self.line_f_en_max.setText("250000")
        else:
            self.line_f_en_max.setText(str(self.energy_bundle[3][1]))
        
        # accept button
        self.push_button = QtGui.QPushButton("OK")
        self.push_button.clicked.connect(self.on_push_button)
        
        # set widgets on the grid
        grid.addWidget(self.lbl_dt, 0, 0)
        grid.addWidget(self.line_dt, 0, 1)
        
        grid.addWidget(self.lbl_min_ev, 1, 0)
        grid.addWidget(self.line_min_ev, 1, 1)
        
        grid.addWidget(self.lbl_max_ev, 2, 0)
        grid.addWidget(self.line_max_ev, 2, 1)
        
        grid.addWidget(self.lbl_sr, 3, 0)
        grid.addWidget(self.check_sr, 3, 1)
        
        grid.addWidget(self.lbl_ss, 4, 0)
        grid.addWidget(self.check_ss, 4, 1)
        
        grid.addWidget(self.lbl_sf, 5, 0)
        grid.addWidget(self.check_sf, 5, 1)
        
        grid.addWidget(self.lbl_r_en, 6, 0)
        grid.addWidget(self.line_r_en_min, 6, 1)
        grid.addWidget(self.line_r_en_max, 6, 2)
        
        grid.addWidget(self.lbl_a_en, 7, 0)
        grid.addWidget(self.line_a_en_min, 7, 1)
        grid.addWidget(self.line_a_en_max, 7, 2)
        
        grid.addWidget(self.lbl_s_en, 8, 0)
        grid.addWidget(self.line_s_en_min, 8, 1)
        grid.addWidget(self.line_s_en_max, 8, 2)
        
        grid.addWidget(self.lbl_f_en, 9, 0)
        grid.addWidget(self.line_f_en_min, 9, 1)
        grid.addWidget(self.line_f_en_max, 9, 2)
        
        grid.addWidget(self.push_button, 10, 1, 1, 2)
        
        self.setLayout(grid)
        
    def on_push_button(self):
        search_properties = record()
        #### FIND R-a (recoil-alpha pairs)
        try:
            search_properties.time_dlt = int(self.line_dt.text()) # microseconds
            search_properties.chain_length_min = int(self.line_min_ev.text())
            search_properties.chain_length_max = int(self.line_max_ev.text())
            search_properties.recoil_first = \
                bool(self.check_sr.checkState() == QtCore.Qt.Checked)
            search_properties.search_side = \
                bool(self.check_ss.checkState() == QtCore.Qt.Checked)
            search_properties.search_fission = \
                bool(self.check_sf.checkState() == QtCore.Qt.Checked)
        except ValueError as e:
            # print(f"ERROR :: SearchChainDialog :: {e}")
            QMessageBox.critical(self, "Error search properties",
                                 f"ERROR :: SearchChainDialog :: {e}")
            # self.reject()
            return
        
        # energy_bundle = [(4000, 20000, 'R'), (9300, 11500, 'a'), ]
        def check_range(min_energy, max_energy, msg=""):
            if min_energy >= max_energy:
                raise ValueError(f"{msg} min energy >= max energy")
                
        try:
            energy_bundle = []
            
            energy_recoil_min = int(self.line_r_en_min.text())
            energy_recoil_max = int(self.line_r_en_max.text())
            check_range(energy_recoil_min, energy_recoil_max, "Recoil")
            energy_bundle.append((energy_recoil_min, energy_recoil_max, "R"))
            
            energy_alpha_min = int(self.line_a_en_min.text())
            energy_alpha_max = int(self.line_a_en_max.text())
            check_range(energy_alpha_min, energy_alpha_max, "Alpha")
            energy_bundle.append((energy_alpha_min, energy_alpha_max, "a"))
            
            energy_side_min = int(self.line_s_en_min.text())
            energy_side_max = int(self.line_s_en_max.text())
            check_range(energy_side_min, energy_side_max, "Side")
            energy_bundle.append((energy_side_min, energy_side_max, "S"))
            
            energy_fission_min = int(self.line_f_en_min.text())
            energy_fission_max = int(self.line_f_en_max.text())
            check_range(energy_fission_min, energy_fission_max, "Fission")
            energy_bundle.append((energy_fission_min, energy_fission_max, "F"))
        except ValueError as e:
            QMessageBox.critical(self, "Error search properties",
                                 f"ERROR :: SearchChainDialog :: {e}")
            # print(f"ERROR :: SearchChainDialog :: {e}")
            # self.reject()
            return
        
        self.result = (search_properties, energy_bundle)
        self.accept()   

### Interface widgets section ---
class CrosshairWidget(pg.GraphicsWindow): 
    """Window widget with fitter of selected area, 
    crosshair showing coordinates of mouse cursor and data 
    (graph name, [x, y] mouse, [mu, sigm] fit) in the right-up corner. 
    All other windows interface components of the application contain this widget.

    Main methods:
    setTitle(title) - set title of the window
    mouseDoubleClickEvent(self, evt) - set event for mouse double click to 
    prosses data from selected area
    Clear() - clear
    setData(x, y) - set data for step graph, len(x) == len(y) + 1

    """
    
    DoubleClickedSignal = QtCore.pyqtSignal(int, name='DoubleClickedSignal')
    
    # fit functions
    @staticmethod
    def gauss(x, *p):
        A, mu, sigm = p
        return A * np.exp(-(x-mu)**2 / (2. * sigm**2))

    @staticmethod
    def fit_data(x, y):
        # initial parameters A, mu, sigm
        p0 = [1., np.sum(x * y) / np.sum(y), 1.]
        good_fit = False
        # fitting
        try:
            coef, corr = curve_fit(CrosshairWidget.gauss, x, y, p0=p0)
        except Exception as e: # there are a lot of fit errors
            print("Fit error: ", e)
            coef = p0
        else:
            good_fit = True

        if good_fit:
            try:
                std_deviations = np.sqrt(np.diag(corr))
            except ValueError:
                good_fit = False
                std_deviations = -np.ones(3)
        else:
            std_deviations = -np.ones(3)

        # even if no errors occured there is possible to be a nan in sigm
        # which is actually a bad fit 
        if np.isnan(std_deviations[0]):
            good_fit = False

        curve = CrosshairWidget.gauss(x, *coef)

        return x, y, curve, good_fit, coef, std_deviations

    def __init__(self, parent=None, id_=None, title='14'):
        pg.GraphicsWindow.__init__(self, parent)
#        if not id_:
#            print """Warning! Without id_ mark, the component can't handle \
#            DoubleClickEvent"""
        self.id = id_
        self.label = pg.LabelItem(justify='left')
        self.addItem(self.label)
        if title:
             self.title = title.strip()   
             self.label.setText(
                 "<span style='font-size: 14pt'><span style='color: yellow'>%s</span>" %
                 (self.title))
        
        #plot white noise as default picture
        self.Plot = self.addPlot(row = 1, col = 0)
        self.vertlabel = self.addLabel('', angle = -90, row = 0)
        #self.Plot.setAutoVisible(y=True)
# 0   'Front Alpha'       [channel/energy, counts] distribution
# 1   'Front Fission'     [channel/energy, counts] distribution
# 2   'Back Alpha'        [channel/energy, counts] distribution   
# 3   'Back Fission'      [channel/energy, counts] distribution
# 4   'Side Alpha'        [channel/energy, counts] distribution
# 5   'Side Fission'      [channel/energy, counts] distribution
# 6   'Front position'    [counts, strip] distribution
# 7   'Back position'     [counts, strip] distribution
# 8   'dE1 - dE2'         [channel, counts] distribution
# 9   'Rotation Time'     [channel, counts] distribution
# 10  'Intensivity timeserie' [time, counts] distribution + chains     
# 11  -                   -
# 12  '3D position'       [front strip, back strip, counts] distribution
  
        init_num = 128
        if id_ < 6:
            init_num = 8000
        elif id_ > 8:
            init_num = 2000
            
        self.x = np.arange(init_num + 1)
        self.y = init_num + \
            15000 * pg.gaussianFilter(np.random.random(size=init_num), 10) +\
            3000 * np.random.random(size=init_num)
        self.curve = self.Plot.plot(self.x, self.y, stepMode = True)#, fillLevel=0, brush=(0, 0, 255, 150))
        
        #cross hair
        vLine = pg.InfiniteLine(angle = 90, movable = False)
        hLine = pg.InfiniteLine(angle = 0, movable = False)
        self.Plot.addItem(vLine, ignoreBounds = True)
        self.Plot.addItem(hLine, ignoreBounds = True) 
        vb = self.Plot.vb

        # region item
        if 'position' not in self.title:
            x_mddl = np.sum(self.x[1:] * self.y) / np.sum(self.y) 
            x_lim = x_mddl-100, x_mddl+100
        else:
            x_lim = [0, 40]
        lr = pg.LinearRegionItem(x_lim)
        lr.setZValue(-10)
        self.fit_info = " " * 10

        def fitter():
            # prepare plot data
            x1, x2 = lr.getRegion()
            x = (self.x[1:] + self.x[:-1]) / 2 # bin centeres
            ind = (x >= int(x1)) & (x <= int(x2))
            x, y = x[ind], self.y[ind]
            # fit by gaussian
            fit = CrosshairWidget.fit_data
            x, y, curve, good_fit, coef, std_deviations = fit(x, y)

            self.fit_info = "ft:[%4.1f(%1.1f), %2.1f(%1.1f)]" %\
                    (round(coef[1], 0), round(abs(std_deviations[1]), 1),\
                     round(coef[2], 1), round(abs(std_deviations[2]), 1))

            # draw fit curve
            try:
                if good_fit:
                    x_bins = prepare_x(x)
                    self.fit_curve.setData(x_bins, curve)
                else:
                    self.fit_info=""
            except AttributeError:
                if good_fit:
                    x_bins = prepare_x(x)
                    self.fit_curve = self.Plot.plot(x_bins, curve,
                        stepMode=True, fillLevel=2,
                        brush=(255, 0, 0, 100))
                print("CHECK bad fit: ", self.fit_info)

        lr.sigRegionChanged.connect(fitter)
        self.Plot.addItem(lr)
        
        # mouse move events
        output_text = """<span style='font-size: 14pt'>\
<span style='color: yellow'>%s <span style='color: red'>\
<span style='font-size: 10pt'>(%0.0f, %0.0f) \
<span style='color: LawnGreen'>%s</span>"""

        def mouseMoved(evt):
            pos = evt[0]  ## using signal proxy turns original arguments into a tuple
            if self.Plot.sceneBoundingRect().contains(pos):
                mousePoint = vb.mapSceneToView(pos)
                x = int(mousePoint.x())
                index = 0
                for x_ in self.x:
                    if x_ >= x:
                        break
                    index += 1
                self.title = self.title.strip()
                if index >= 0 and index < len(self.y):
                    self.label.setText(output_text % (self.title, \
                            self.x[index], self.y[index], self.fit_info))
#                    print "CHECK mm: ", self.title, \
#                            self.x[index], self.y[index], self.fit_info

                vLine.setPos(mousePoint.x())
                hLine.setPos(mousePoint.y())
                
        #self.Plot.scene().sigMouseMoved.connect(mouseMoved)        
        self.proxy = pg.SignalProxy(self.Plot.scene().sigMouseMoved, 
                                    rateLimit=60, slot=mouseMoved)
        
    def setData(self, x, y):
        self.x, self.y = x, y
        try:
            if type(y) == pd.Series:
                #if self.id in [6, 7]:
                if len(x) == len(y):
                    self.x = prepare_x(x)
                    #self.x = np.r_[x, 2 * x[-1] - x[-2]
                self.y = y.to_numpy()
            self.curve.setData(self.x, self.y)
        except Exception as e:
            print(cur_time(), ' :: Error setData: ', e)
            print(self.parent.__repr__())
            print('Widget ID: ', self.id)
            print('-' * 30)
        #self.curve.setData(x, y[1:])
        
    def Clear(self):
        try:
            self.curve.clear()
        except Exception:
            pass #some inner pyqtgraph error while use clear for stepMode 
        
    def setTitle(self, title):
        assert type(title) is str
        self.title = title
        self.label.setText("<span style='font-size: 14pt'><span style='color: yellow'>%s</span>" % (self.title))
        
    def mouseDoubleClickEvent(self, evt):
        if self.id is not None:
            self.DoubleClickedSignal.emit(self.id)
        super(pg.GraphicsWindow, self).mouseDoubleClickEvent(evt)
    
    def setXRange(self, xmin, xmax):
        self.Plot.setXRange(xmin, xmax)
        
    def setYRange(self, ymin, ymax):
        self.Plot.setYRange(ymin, ymax)
  

class CrosshairWidget_tof(CrosshairWidget): 
    """
    See Crosshair description. The same except support no fit and
    allow to plot two tof plots.
    
    Main methods:
    setTitle(title) - set title of the window
    mouseDoubleClickEvent(self, evt) - no effect
    Clear() - clear
    setData(x, y, name='tofD1') - set data for one of two tof graphs
                'tofD1' either 'tofD2'
                , len(x) == len(y) + 1

    """
    
    DoubleClickedSignal = QtCore.pyqtSignal(int, name='DoubleClickedSignal')

    def __init__(self, parent=None, id_=None, title='14'):
        pg.GraphicsWindow.__init__(self, parent)
#        if not id_:
#            print """Warning! Without id_ mark, the component can't handle \
#            DoubleClickEvent"""
        self.id = id_
        self.label = pg.LabelItem(justify='left')
        self.addItem(self.label)
        if title:
             self.title = title.strip()   
             self.label.setText(
                 "<span style='font-size: 14pt'><span style='color: yellow'>%s</span>" %
                 (self.title))
        
        #plot white noise as default picture
        self.Plot = self.addPlot(row = 1, col = 0)
        self.vertlabel = self.addLabel('', angle = -90, row = 0)
        #self.Plot.setAutoVisible(y=True)
# 0   'Front Alpha'       [channel/energy, counts] distribution
# 1   'Front Fission'     [channel/energy, counts] distribution
# 2   'Back Alpha'        [channel/energy, counts] distribution   
# 3   'Back Fission'      [channel/energy, counts] distribution
# 4   'Side Alpha'        [channel/energy, counts] distribution
# 5   'Side Fission'      [channel/energy, counts] distribution
# 6   'Front position'    [counts, strip] distribution
# 7   'Back position'     [counts, strip] distribution
# 8   'dE1 - dE2'         [channel, counts] distribution
# 9   'Rotation Time'     [channel, counts] distribution
# 10  'Intensivity timeserie' [time, counts] distribution + chains     
# 11  -                   -
# 12  '3D position'       [front strip, back strip, counts] distribution
  
        init_num = 2000
            
        self.x1 = np.arange(init_num + 1)
        self.y1 = init_num + \
            15000 * pg.gaussianFilter(np.random.random(size=init_num), 10) +\
            3000 * np.random.random(size=init_num)
        self.x2 = np.arange(init_num + 1)
        self.y2 = init_num + \
            15000 * pg.gaussianFilter(np.random.random(size=init_num), 10) +\
            3000 * np.random.random(size=init_num)
        self.curve1 = self.Plot.plot(self.x1, self.y1, 
            stepMode = True, name='tofD1', fillLevel=2,
            brush=(255, 0, 0, 40))#, fillLevel=0, brush=(0, 0, 255, 150))
        self.curve2 = self.Plot.plot(self.x2, self.y2, 
            stepMode = True, name='tofD2', fillLevel=2,
            brush=(0, 255, 0, 40))
        
        #cross hair
        vLine = pg.InfiniteLine(angle = 90, movable = False)
        hLine = pg.InfiniteLine(angle = 0, movable = False)
        self.Plot.addItem(vLine, ignoreBounds = True)
        self.Plot.addItem(hLine, ignoreBounds = True) 
        vb = self.Plot.vb
        
        # mouse move events
        output_text = """<span style='font-size: 14pt'>\
<span style='color: yellow'>%s <span style='color: red'>\
<span style='font-size: 10pt'>(%0.0f, dE1: %0.0f, dE2: %0.0f) \
</span>"""

        def mouseMoved(evt):
            pos = evt[0]  ## using signal proxy turns original arguments into a tuple
            if self.Plot.sceneBoundingRect().contains(pos):
                mousePoint = vb.mapSceneToView(pos)
                x = int(mousePoint.x())
                index1 = 0
                index2 = 0
                
                for x_ in self.x1:
                    if x_ >= x:
                        break
                    index1 += 1
                    
                for x_ in self.x2:
                    if x_ >= x:
                        break
                    index2 += 1
                    
                self.title = self.title.strip()
                if (index1 >= 0 and index1 < len(self.y1)) and \
                      (index2 >= 0 and index2 < len(self.y2)):
                    self.label.setText(
                        output_text % (self.title, 
                        self.x1[index1], self.y1[index1], 
                        self.y2[index2])
                    )
#                    print "CHECK mm: ", self.title, \
#                            self.x[index], self.y[index], self.fit_info

                vLine.setPos(mousePoint.x())
                hLine.setPos(mousePoint.y())
                
        #self.Plot.scene().sigMouseMoved.connect(mouseMoved)        
        self.proxy = pg.SignalProxy(self.Plot.scene().sigMouseMoved, 
                                    rateLimit=60, slot=mouseMoved)
        
    def setData(self, x, y, name='tofD1'):
        if name == 'tofD1':
            attr_x = 'x1'
            attr_y = 'y1'
            curve = 'curve1'
        elif name == 'tofD2':
            attr_x = 'x2'
            attr_y = 'y2'
            curve = 'curve2'
        else:
            raise ValueError(f"Wrong input name in {self}.setData")
            
        setattr(self, attr_x, x)
        setattr(self, attr_y, y)
        try:
            if type(y) == pd.Series:
                #if self.id in [6, 7]:
                if len(x) == len(y):
                    setattr(self, attr_x, prepare_x(x))
                    #self.x = np.r_[x, 2 * x[-1] - x[-2]
                setattr(self, attr_y, y.to_numpy())
            x = attrgetter(attr_x)(self)
            y = attrgetter(attr_y)(self)
            attrgetter(curve)(self).setData(x, y)
        except Exception as e:
            print(cur_time(), ' :: Error setData: ', e)
            print(self.parent.__repr__())
            print('Widget ID: ', self.id)
            print('-' * 30)
            
    def Clear(self):
        try:
            self.curve1.clear()
            self.curve2.clear()
        except Exception:
            pass #some inner pyqtgraph error while use clear for stepMode
        
    def mouseDoubleClickEvent(self, evt):
        pass
    
    
class CrosshairWidget_timeserie(CrosshairWidget): 
    """
    Widget to work with intensivity timeserie and show found correlations
    chains.
    
    Main methods:
    setTitle(title) - set title of the window
    mouseDoubleClickEvent(self, evt) - no effect
    Clear() - clear
    plot_timeserie(x, y) - plot timeserie line
    plot_chains(coords) - plot chain events marks

    """
    
    DoubleClickedSignal = QtCore.pyqtSignal(int, name='DoubleClickedSignal')

    def __init__(self, parent=None, id_=None, title='14'):
        pg.GraphicsWindow.__init__(self, parent)
#        if not id_:
#            print """Warning! Without id_ mark, the component can't handle \
#            DoubleClickEvent"""
        self.id = id_
        self.label = pg.LabelItem(justify='left')
        self.addItem(self.label)
        if title:
             self.title = title.strip()   
             self.label.setText(
                 "<span style='font-size: 14pt'><span style='color: yellow'>%s</span>" %
                 (self.title))
        
        #plot white noise as default picture
        self.Plot = self.addPlot(row = 1, col = 0)
        self.vertlabel = self.addLabel('', angle = -90, row = 0)
        #self.Plot.setAutoVisible(y=True)
# 0   'Front Alpha'       [channel/energy, counts] distribution
# 1   'Front Fission'     [channel/energy, counts] distribution
# 2   'Back Alpha'        [channel/energy, counts] distribution   
# 3   'Back Fission'      [channel/energy, counts] distribution
# 4   'Side Alpha'        [channel/energy, counts] distribution
# 5   'Side Fission'      [channel/energy, counts] distribution
# 6   'Front position'    [counts, strip] distribution
# 7   'Back position'     [counts, strip] distribution
# 8   'dE1 - dE2'         [channel, counts] distribution
# 9   'Rotation Time'     [channel, counts] distribution
# 10  'Intensivity timeserie' [time, counts] distribution + chains     
# 11  -                   -
# 12  '3D position'       [front strip, back strip, counts] distribution
            
        self.curve1 = None
        self.curve2 = None
        
        #cross hair
        vLine = pg.InfiniteLine(angle = 90, movable = False)
        hLine = pg.InfiniteLine(angle = 0, movable = False)
        self.Plot.addItem(vLine, ignoreBounds = True)
        self.Plot.addItem(hLine, ignoreBounds = True) 
        vb = self.Plot.vb
        
        # mouse move events
        output_text = """<span style='font-size: 14pt'>\
<span style='color: yellow'>%s <span style='color: red'>\
<span style='font-size: 10pt'>(%0d min, Counts: %0.0f) \
</span>"""

        def mouseMoved(evt):
            if (self.curve1 is None):
                return
            
            pos = evt[0]  ## using signal proxy turns original arguments into a tuple
            if self.Plot.sceneBoundingRect().contains(pos):
                mousePoint = vb.mapSceneToView(pos)
                x = int(mousePoint.x())
                
                index1 = 0
                for x_ in self.x:
                    if x_ >= x:
                        break
                    index1 += 1
                    
                self.title = self.title.strip()
                if (index1 >= 0 and index1 < len(self.y)):
                    self.label.setText(
                        output_text % (self.title, 
                        self.x[index1], self.y[index1])
                    )
#                    print "CHECK mm: ", self.title, \
#                            self.x[index], self.y[index], self.fit_info

                vLine.setPos(mousePoint.x())
                hLine.setPos(mousePoint.y())
                
        #self.Plot.scene().sigMouseMoved.connect(mouseMoved)        
        self.proxy = pg.SignalProxy(self.Plot.scene().sigMouseMoved, 
                                    rateLimit=60, slot=mouseMoved)
        
    def plot_timeserie(self, x, y):
        self.curve1 = self.Plot.plot(x, y)
        self.Plot.setXRange(min(x), max(x))
        self.x = x
        self.y = y 
        
    def plot_chains(self, coords):
        x = [i for i, _ in coords]
        y = [j for _, j in coords]
        
        # rescale on Y-axis
        index1 = 0
        for x_ in self.x:
            if x_ >= x[0]:
                break
            index1 += 1
        #                  
        height = None
        if (index1 >= 0 and index1 < len(self.y)):
            height = self.y[index1]
        if height:
            y = [i * height / max(y) for i in y]
        
        self.curve2 = self.Plot.plot(x, y, pen=None,
            symbol='t', symbolPen=None,
            symbolSize=20,
            symbolBrush=(255, 255, 0, 200)
        )
                  
    def Clear(self):
        try:
            self.curve1.clear()
            self.curve2.clear()
        except Exception:
            pass #some inner pyqtgraph error while use clear for stepMode
        
    def mouseDoubleClickEvent(self, evt):
        pass
    
        
class CHMixin(object):
    
    def setData(self, row, col, x, y, **argv):
        self.plotslist[row][col].setData(x, y, **argv)
               
    def setTitle(self, row, col, title):
        self.plotslist[row][col].setTitle(title)
        
    def Clear(self):
        rows, cols = list(range(len(self.plotslist))), list(range(len(self.plotslist[0])))
        for row_ in rows:
            for col_ in cols:
                
                # missed widget, space owned by timeserie
                if (row_ == 5) & (col_ == 1):
                    continue
                
                self.plotslist[row_][col_].Clear()
        
    def setXRange(self, row, col, xmin, xmax):
        self.plotslist[row][col].setXRange(xmin, xmax) 
        
    def setYRange(self, row, col, ymin, ymax):
        self.plotslist[row][col].setYRange(ymin, ymax)
             
 
class SingleWidget(QtGui.QWidget):
    """Class wrapping only one single Crosshair widget"""

    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self._plotslist = []
        self._plotslist.append(CrosshairWidget(id_ = 0)) 
        
        grid = QtGui.QGridLayout()
        grid.addWidget(self._plotslist[0], 0, 0)
        self.setLayout(grid) 
        
    def setData(self, x, y):
        self._plotslist[0].setData(x, y)
               
    def setTitle(self, title):
        self._plotslist[0].setTitle(title)
        
    def Clear(self):
        self._plotslist[0].Clear()
        
    def setXRange(self, xmin, xmax):
        self._plotslist[0].setXRange(xmin, xmax) 
        
    def setYRange(self, ymin, ymax):
        self._plotslist[0].setYRange(ymin, ymax)

           
class PrimaryWidget(QtGui.QWidget, CHMixin):
    '''
    The widget represents main overwiew set of plots, containing Alpha/Fission Front/Back/Side, Position and Time distributions.
    Property:
        plotslist[row][col] 
    Methods:
        setData(row, col, x, y)
        setTitle(row, col, title)
        setXRange(xmin, xmax)
    '''
    #structrure 12 plots
    def makeTitleslist(self):
        Titleslist = [ [0]*2 for i in range(6) ]
        Titleslist[0][0] = 'Front Alpha'
        Titleslist[0][1] = 'Front Fission'
        Titleslist[1][0] = 'Back Alpha'
        Titleslist[1][1] = 'Back Fission'
        Titleslist[2][0] = 'Side Alpha'
        Titleslist[2][1] = 'Side Fission'
        Titleslist[3][0] = 'Front position'
        Titleslist[3][1] = 'Back position'
        Titleslist[4][0] = 'dE1 - dE2'
        Titleslist[4][1] = 'Rotation Time'
        Titleslist[5][0] = 'Intensivity timeserie'
        Titleslist[5][1] = ''
        return Titleslist
    
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.plotslist = []
        self.Titleslist = self.makeTitleslist()
        grid = QtGui.QGridLayout()
        
        #add 12 plots in Г shape
        for i in range(4):
            self.plotslist.append([])
            for j in range(2):
                self.plotslist[i].append(
                    CrosshairWidget(id_ = i * 2 + j, 
                                    title=self.Titleslist[i][j]))
                grid.addWidget(self.plotslist[i][j], i, j)
                if (not (i == 0) & (j ==0 )) and (i < 3):
                    if j % 2 == 0:
                        self.plotslist[i][j].Plot.setXLink(
                                        self.plotslist[0][0].Plot)
                    elif j % 2 == 1:
                        self.plotslist[i][j].Plot.setXLink(
                                        self.plotslist[0][1].Plot)
                        
        for i in range(4, 6):
            self.plotslist.append([])
            for j in range(2):
                
                # add dE1 - dE2 widget
                if (i == 4) & (j == 0):
                    self.plotslist[i].append(
                        CrosshairWidget_tof(id_ = i * 2 + j, 
                            title = self.Titleslist[i][j])
                    )
                    grid.addWidget(self.plotslist[i][j], i - 4, j + 2)
                    continue
                
                # add intensivity timeserie
                if (i == 5) & (j == 0):
                    self.plotslist[i].append(
                        CrosshairWidget_timeserie(id_ = i * 2 + j, 
                            title = self.Titleslist[i][j])
                    )
                    grid.addWidget(self.plotslist[i][j], i - 4, j + 2, 1, 2)
                    continue
                
                if (i == 5) & (j == 1):
                    continue
                
                self.plotslist[i].append(
                    CrosshairWidget(id_ = i * 2 + j, 
                                    title = self.Titleslist[i][j]))
                grid.addWidget(self.plotslist[i][j], i - 4, j + 2)
         
        #add 3D plot window 
#        self.widget3D = gl.GLViewWidget()
#        self.widget3D.setCameraPosition(distance=50)
#        g = gl.GLGridItem()
#        g.scale(2, 2, 1)
#        g.setDepthValue(10)  # draw grid after surfaces since they may be translucent
#        self.widget3D.addItem(g)
        
        #3D plot
        self.widget3D = gl.GLViewWidget()
        self.widget3D.show()
        #w.setWindowTitle('pyqtgraph example: GLSurfacePlot')
        self.widget3D.setCameraPosition(distance = 140,
                                        elevation = 30, azimuth = 50)#pos_spectr.data.max()+20)
        #grid
        gx = gl.GLGridItem()
        gx.rotate(90, 0, 1, 0)
        gx.setSize(8, 48)
        gx.translate(-64, 0, 4)
        self.widget3D.addItem(gx)
        gy = gl.GLGridItem()
        gy.rotate(90, 1, 0, 0)
        gy.setSize(128, 8)
        gy.translate(0, -24, 4)
        self.widget3D.addItem(gy)
        gz = gl.GLGridItem()
        gz.setSize(128, 48)
        gz.translate(0, 0, 0)
        self.widget3D.addItem(gz)
        
        z = np.zeros((128, 48)) #=pos_spectr.data.to_numpy()*8/float(pos_spectr.data.max().max()
        self.plot3D = gl.GLSurfacePlotItem(z = z,
                                           shader = 'heightColor', 
                                           glOptions = 'opaque',
                                           computeNormals = False)
        #color=(0.6, 0.7, 1, 1)#, shader='shaded', glOptions='opaque')# shader='shaded', color=(0.6, 0.7, 1, 1))
        #color=(1, 0, 0, 1), shader='heightColor', glOptions='opaque')
        self.plot3D.shader()['colorMap'] = \
            np.array([0., 0.2, 1, 0.2, 0.2, 2.15, 0.1, 1, 0.5])#np.array([0.2, 2, 0.5, 0.2, 1, 1, 0.2, 0, 2])
        #p1.scale(16./49., 16./49., 1.0)
        self.plot3D.translate(-64, -24, 0)
        self.widget3D.addItem(self.plot3D)

        #example 3d picture
#        cols = 90
#        rows = 100
#        x = np.linspace(-18, 18, cols+1).reshape(cols+1, 1)
#        y = np.linspace(-18, 18, rows+1).reshape(1, rows+1)
#        d = (x**2 + y**2) * 0.1
#        d2 = d ** 0.5 + 0.1
#        ## precompute height values for all frames
#        phi = np.arange(0, np.pi*2, np.pi/20.)
#        self.z = np.sin(d[np.newaxis,...] + phi.reshape(phi.shape[0], 1, 1)) / d2[np.newaxis,...]
#        ## create a surface plot, tell it to use the 'heightColor' shader
#        ## since this does not require normal vectors to render (thus we 
#        ## can set computeNormals=False to save time when the mesh updates)
#        self.p4 = gl.GLSurfacePlotItem(x=x[:, 0], y = y[0,:], shader='heightColor', computeNormals=False, smooth=False)
#        self.p4.shader()['colorMap'] = np.array([0.2, 2, 0.5, 0.2, 1, 1, 0.2, 0, 2])
#        #self.p4.translate(0, 0, 0)
#        self.index = 0
#        self.widget3D.addItem(self.p4)
#        self.p4.setData(z=self.z[self.index%self.z.shape[0]])
##        def update():
##            self.index -= 1
##            self.p4.setData(z=self.z[self.index%self.z.shape[0]])
        grid.addWidget(self.widget3D, 2, 2, 2, 2)                   
        self.setLayout(grid) 
        
    def setData3D(self, pos_spectr): # for PosSpectrum 
        if pos_spectr is None:
            return
        self.plot3D.setData(z=pos_spectr.data.to_numpy() * 8 / \
                            float(pos_spectr.data.max().max()))
                              #pos_spectr.data.to_numpy()*8/float(pos_spectr.data.max().max())
        
class BoxWidget(QtGui.QWidget, CHMixin):
    '''
    The widget represents 16 linked crosshair-plots.
    Property:
        plotslist[row][col] - array of 16 (4x4) crosshair widgets
    Methods:
        setData(row, col, x, y)
        setTitle(row, col, title)
        setXRange(xmin, xmax)
    '''
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.plotslist = []
        grid = QtGui.QGridLayout()
        for i in range(4):
            self.plotslist.append([])
            for j in range(4):
                self.plotslist[i].append(CrosshairWidget(id_ = i * 4 + j))
                grid.addWidget(self.plotslist[i][j], i, j)
                if not (i ==0 ) & (j == 0):
                    self.plotslist[i][j].Plot.setXLink(self.plotslist[0][0].Plot)
                    #self.plotslist[i][j].Plot.setYLink(self.plotslist[0][0].Plot)
                    
        self.setLayout(grid)
               
        
class SideWidget(QtGui.QWidget):
    '''
    The widget contains a button pannel to switch viewer plots (by 16 stips or 1 by 1),
    parametertree to set viewer parameters and to see filename and count rate.
    The widget communicate with VisualizeManager object by signals emmiting from button "Accept" 
    and buttons of Control Panel.
    '''
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.parent = parent
        #init parametertree and a button
        self.parametertree = pg.parametertree.ParameterTree()
        
        #set control panel for swithing plots
        self.controlpanel = QtGui.QWidget()
        box = QtGui.QGridLayout()
        self.buttonback = QtGui.QPushButton('To main window')
        self.buttonleft = QtGui.QPushButton('<')
        self.buttonright = QtGui.QPushButton('>')
        self.buttonleft16 = QtGui.QPushButton('<<')
        self.buttonright16 = QtGui.QPushButton('>>')
        #
        box.addWidget(self.buttonback, 0, 0, 1, 4, alignment=QtCore.Qt.AlignCenter)
        box.addWidget(self.buttonleft16, 1, 0)
        box.addWidget(self.buttonleft, 1, 1)
        box.addWidget(self.buttonright, 1, 2)
        box.addWidget(self.buttonright16, 1, 3)
        #
        self.controlpanel.setLayout(box)
        
        #set layout
        vbox = QtGui.QVBoxLayout()
        vbox.addWidget(self.controlpanel)
        vbox.addWidget(self.parametertree)
        self.setLayout(vbox)

### User event section ---
def createNewEvent():
    
    class NewEvent(QtCore.QEvent):
        
        idType = QtCore.QEvent.registerEventType()
           
        def __init__(self, data):
            
            # QtCore.QEvent.__init__(self, self.idType)
            super().__init__(self.idType)
            self.data = data
            
    return NewEvent

NewDataEvent = createNewEvent() #this event is to send data from ReadDataThread to the ReaderClass
NewDataTableEvent = createNewEvent() # this event is to send data from ReadDataTableThread to the ReaderClass
PlotUpdateEvent = createNewEvent() #this event is to send data from ReaderClass to the VisualizeManager  
GetNewFiles = createNewEvent()
  
a, b, c = None, None, None
### Read data section ---

def get_read_function(name):
    basename = os.path.basename(name)
    if 'bin' in basename:
        return read_binary.read_file
    elif 'gns' in basename:
        return read_analog.read_file
    elif 'Am' in basename:
        return read_analog.read_file
    elif re.match('[a-zA-Z]+\.[0-9]+', basename):
        return read_analog.read_file
    else:
        return read_analog.read_file
        print("Unknown file format of the filename: " + name)
        
              
class ReadDataThread(QtCore.QThread):
    '''
    This class reads experimental data from files into separate thread so 
    it doesn't interrupt animation in the main window of application.
    Read data files by chunks, calculate distributions histograms and 
    return spectum classes packed in dict.
    It uses import new_data_processing.tools.read_file module to deal with 
    experimental data.
    '''
    
    def __init__(self, queue):
        QtCore.QThread.__init__(self)
        self.queue = queue
        self.flag = True
        
    def run(self):
        while self.flag:
            obj = self.queue.get()
            data = self.read_data(obj)
            if data is not None:
                QtCore.QCoreApplication.postEvent(obj, NewDataEvent(data))
            
    def read_data(self, obj):
        
        print(cur_time(), ' :: Check :: call ReadDataThread.read_data')
        # in case of FTP_mode it is only a piece of file downloaded with size = filesize - offset        
        offset = obj.offset if not obj.FTP_mode else 0
        try:
            read = get_read_function(obj.filename)
        except IOError as e: # error reading filename
            print(e)
        except AttributeError as e: # error importing wrong reading module
            print("Error! Check imported reading modules: " + \
                  "read_analog / read_binary", e)  
        
        # global check_a, check_b
        # check_a = read
        # check_b = (obj.filename, True, offset)
        filename = bytes(obj.filename, 'utf-8')
        try:
            frame, gotten_size = read(filename, strip_convert=True, 
                                      offset=offset)  
        except IOError as e:
            print("!EXCEPTION: ", e)
            return None
        
        if obj.FTP_mode & (gotten_size == 0): # The downloaded file has zero size.
            return None
        elif (gotten_size == obj.offset): # note: read_function returns the total size of the file(!) - gotten_size
            return None
            
        data = obj.initialize_data() 
        data['offset'] = gotten_size 
        if gotten_size > 0:
            data['current_seconds'] = (frame['time_micro'][-1] - frame['time_micro'][0]) // 10 ** 6
            data['current_counts'] = np.sum(frame['event_type'] < 3) // 4 \
                             + np.sum(frame['event_type'] == 3) // 2
            data['seconds'] = data['current_seconds']
            data['counts'] = data['current_counts']
        else:
            data['current_seconds'] = 0
            data['current_counts'] = 0
            
        for tof_ in obj.dlt_E: 
            for scale_ in obj.Scale:
                options = dict(list(zip(['coefs_folder', 'energy_scale', 'tof', 'scale', 'bins'],\
                                [obj.coefs_folder, obj.energy_scale, tof_, scale_, obj.bins])))
                #### channel/energy distributions
                try:
                    data['Spectrum']['Front'][tof_][scale_] = sp.Spectrum(frame, event_type='front', **options)
                    data['Spectrum']['Back'][tof_][scale_] = sp.Spectrum(frame, event_type='back', **options)
                    data['Spectrum']['Side'][tof_][scale_] = sp.Spectrum(frame, event_type='side', **options)
                    data['PosSpectrum'][tof_][scale_] = sp.PositionSpectrum(frame, **options)
                    data['PosSpectrum1D']['Front'][tof_][scale_] = data['PosSpectrum'][tof_][scale_].get_front_distribution()
                    data['PosSpectrum1D']['Back'][tof_][scale_] = data['PosSpectrum'][tof_][scale_].get_back_distribution()
                except Exception as e:
                    print(cur_time(), ' :: Error read_data: ', e)
                    print(tof_, scale_)
        data['Tof_counters']['tofD1'] = sp.TofSpectrum(frame, tof_counter='tofD1')
        data['Tof_counters']['tofD2'] = sp.TofSpectrum(frame, tof_counter='tofD2')
        data['Rotation_time'] = sp.RotTimeSpectrum(frame)
        print(cur_time(), ' Check:: call ReadDataThread.read_data :: got new data')
        del frame
        gc.collect()
        return data
   
   
class ReadDataTableThread(QtCore.QThread):
    '''
    This class reads experimental data from files into separate thread 
    so it doesn't interrupt animation in the main window of application.
    Read data files as data tables, calculate intensivity spectrums and
    perform searching of correlated events chains.
    It uses import new_data_processing.tools.read_file module to deal with experimental data.
    '''
    
    def __init__(self, queue):
        QtCore.QThread.__init__(self)
        self.queue = queue
        self.flag = True
        
    def run(self):
        while self.flag:
            obj = self.queue.get()
            data = self.read_data(obj)
            if data is not None:
                QtCore.QCoreApplication.postEvent(obj, NewDataTableEvent(data))
            
    def read_data(self, obj):
        
        print(cur_time(), ' :: Check :: call ReadDataThread.read_data')
        # in case of FTP_mode it is only a piece of file downloaded with size = filesize - offset        
        # offset = obj.offset if not obj.FTP_mode else 0
        try:
            read = lambda fname: \
                read_analog.get_data_table(fname, 
                                           coefs_folder=obj.coefs_folder) #get_read_data_table_function(obj.filename)
        except IOError as e: # error reading filename
            print(e)
        except AttributeError as e: # error importing wrong reading module
            print("Error! Check imported reading modules: " + \
                  "read_analog / read_binary", e)  
        
        # global check_a, check_b
        # check_a = read
        # check_b = (obj.filename, True, offset)
        filename = bytes(obj.filename, 'utf-8')
        try:
            frame, gotten_size = read(filename)  
        except IOError as e:
            print("!EXCEPTION: ", e)
            return None
         
        if obj.FTP_mode & (gotten_size == 0): # The downloaded file has zero size.
            return None
        elif (gotten_size == 0): # note: read_function returns the total size of the file(!) - gotten_size
            return None
            
        data = obj.initialize_data_dt() 
        data['Intensivity'] = record()
        data['Intensivity'].bins, data['Intensivity'].data = \
            sp.get_intensivity(frame)          
        
        if (obj.search_properties is not None) and \
              (obj.energy_bundle is not None):
                  
            res = search_chain(filename, frame,
                    search_properties=obj.search_properties,
                    energy_bundle=obj.energy_bundle)
            
        else:
            res = search_chain(filename, frame)
            
        if res is not None:
            data['Chains'] = record()
            data['Chains'].coords, data['Chains'].report = res
            
        print(cur_time(), ' Check:: call ReadDataTableThread.read_data :: got new data')
        # del frame
        gc.collect()
        return data
    
    
class ReaderClass(QtCore.QObject):
    '''
    This class contains data for visualization and controls flow of reading data 
    and sending it to VisualManager class.
    '''
    GettingDataSignal = QtCore.pyqtSignal(bool, name='GettingDataSignal')
    GettingDataTableSignal = \
        QtCore.pyqtSignal(bool, name='GettingDataTableSignal')
    GetNewData = QtCore.pyqtSignal(bool, name='GetNewData')
    GetNewDataTable = QtCore.pyqtSignal(bool, name='GetNewDataTable')
    
    def __init__(self, parent=None):
        QtCore.QObject.__init__(self, parent)
        self.parent = parent
        self.init_parameters()
        self.data = None#self.initialize_data()
        self.data_dt = None
        
        #read data thread
        self.queue = queue.Queue()
        self.readthread = ReadDataThread(self.queue)
        self.readthread.start()
        
        # read data table thread
        self.dt_queue = queue.Queue()
        self.dt_readthread = ReadDataTableThread(self.dt_queue)
        self.dt_readthread.start()
        
        #timers
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.send_task_to_thread)
        
        self.timer_dt = QtCore.QTimer()
        self.timer_dt.timeout.connect(self.send_task_to_dt_thread)
        
    def get_task(self):
        new = record()
        new.filename = self.filename
        new.FTP_mode = self.FTP_mode
        new.offset = self.offset
        new.initialize_data = self.initialize_data
        new.coefs_folder = self.coefs_folder
        new.energy_scale = self.energy_scale
        new.bins = self.bins
        new.dlt_E = self.dlt_E
        new.Scale = self.Scale
        return new
        
    def init_parameters(self):
        self.filename = '' #name of the file to read
        self.FTP_mode = False
        self.ftp_dialog = None
        self.coefs_folder = os.path.join('coefs', 'channel') # The folder should contain next files with calibration coefficients: front_alpha.txt, front_fission.txt, back_alpha.txt, back_fission.txt, side_alpha.txt, side_fission.txt 
        self.offset = 0
        self.dlt_E = (True, False, 'all')
        self.Scale = ('alpha', 'fission')
        self.energy_scale = True
        self.bins = None    
        
        search_properties = record()
        #
        search_properties.time_dlt = 21 * 10**6 # microseconds
        search_properties.chain_length_min = 3
        search_properties.chain_length_max = 10
        search_properties.recoil_first = True
        search_properties.search_side = True
        search_properties.search_fission = True
        self.search_properties = search_properties
        
        energy_bundle = [
            (6000, 15000, 'R'), (7790, 12600, 'a'), 
            (7600, 12000, 'S'), (40000, 300000, 'F')
        ] # order matters! 
        self.energy_bundle = energy_bundle
        
    def activateFtpMode(self, ftp_dialog):
        self.ftp_dialog = ftp_dialog
        self.FTP_mode = True
    
    def disableFtpMode(self):
        self.ftp_dialog = None
        self.FTP_mode = False
        
    @staticmethod     
    def initialize_data():
        #data containers
        data = {'Spectrum': None, 'PosSpectrum': None, 'PosSpectrum1D': None,
                'TimeSpectrum': None, 'Tof_counters': None}
        Tof_dict = {False: None, True: None, 'all': None}
        Scale_dict = {'alpha': None, 'fission': None}
        
        # main energy/channel spectrums. Structure: Spectrum[Detectors][Tof][scale]
        # detectors: ('Front'/'Back'/'Side'), Tof:(True/False/'all'), scale: ('alpha'/'fission') 
        Spectrum = {'Front': None, 'Back': None, 'Side': None}
        for key in list(Spectrum.keys()):
            Spectrum[key] = dict(Tof_dict)
            d_ = Spectrum[key]
            for key1 in list(d_.keys()):
                Spectrum[key][key1] = dict(Scale_dict)
                
        # 2D position spectrums. Structure: Spectrum[Tof][scale]
        # Tof:(True/False/'all'), scale: ('alpha'/'fission')  
        PosSpectrum = dict(Tof_dict)
        for key in list(PosSpectrum.keys()):
            PosSpectrum[key] = dict(Scale_dict) 
        
        # 1D position spectrums. Structure: Spectrum[Detectors][Tof][scale]
        # detectors: ('Front'/'Back'), Tof:(True/False/'all'), scale: ('alpha'/'fission')
        PosSpectrum1D = {'Front':None,'Back':None}
        for key in list(PosSpectrum1D.keys()):
            PosSpectrum1D[key] = dict(Tof_dict)
            d_ = PosSpectrum1D[key]
            for key1 in list(d_.keys()):
                PosSpectrum1D[key][key1] = dict(Scale_dict)
        #
        data['Spectrum'] = Spectrum
        data['PosSpectrum'] = PosSpectrum
        data['PosSpectrum1D'] = PosSpectrum1D
        
        #dltE1, dltE2, Tof spectrums
        Tof_counters = {'tofD1': None, 'tofD2': None}
        data['Tof_counters'] = Tof_counters
        
        # rotation time
        data['Rotation_time'] = None
        
        # time, counts
        data['current_seconds'] = 0
        data['current_counts'] = 0
        data['seconds'] = 0
        data['counts'] = 0
        
        return data

    @staticmethod     
    def initialize_data_dt():
        #data containers
        data = {'Intensivity': None, 'Chains': None}
    
        return data
    
    def send_task_to_thread(self):
        '''
        Send task to queue to make the thread read the data.
        The function post the object to get an answer from the thread.
        '''
        self.GettingDataSignal.emit(True) # send signal to turn off the indicator IsUpdated on ControlPanel (side bottom)
         #if FTP_mode - download from outer host
        if self.FTP_mode: 
            self.ftp_dialog.download()#offset=obj.offset) 
            print(cur_time(), " :: CHECK: download data from host", self.ftp_dialog.sftp.host)
        self.queue.put(self)#.get_task())  
        
    def send_task_to_dt_thread(self):
        '''
        Send task to queue to make the thread read the data.
        The function post the object to get an answer from the thread.
        '''
        self.GettingDataTableSignal.emit(True)
        #if FTP_mode - download from outer host
        if self.FTP_mode: 
            self.ftp_dialog.download()#offset=obj.offset) 
            print(cur_time(), " :: CHECK: download data from host", self.ftp_dialog.sftp.host)
        self.dt_queue.put(self)#.get_task())  
        
    def customEvent(self, e): #get data from the ReadDataThread. The method is inherited from QtCore.QObject
        if e.type() == NewDataEvent.idType:
            self.get_data(e.data)
        elif e.type() == NewDataTableEvent.idType:
            self.get_data_dt(e.data)
      
    def get_data(self, new_data):
        
        if new_data is None:
            return
        self.offset = new_data['offset']
        if self.data is None:
            self.data = new_data
        else:
            for tof_ in self.dlt_E:
                for scale_ in self.Scale:
                    if tof_ and (scale_=='fission'):
                        continue
                    # if new_data['Spectrum']['Front'][tof_][scale_] is not None:
                    self.data['Spectrum']['Front'][tof_][scale_] += \
                        new_data['Spectrum']['Front'][tof_][scale_]
                    # if new_data['Spectrum']['Back'][tof_][scale_] is not None:
                    self.data['Spectrum']['Back'][tof_][scale_] += \
                        new_data['Spectrum']['Back'][tof_][scale_] 
                    # if new_data['Spectrum']['Side'][tof_][scale_] is not None:
                    self.data['Spectrum']['Side'][tof_][scale_] += \
                        new_data['Spectrum']['Side'][tof_][scale_] 
                    # if new_data['PosSpectrum'][tof_][scale_] is not None:
                    self.data['PosSpectrum'][tof_][scale_] += \
                        new_data['PosSpectrum'][tof_][scale_] 
                
                    self.data['PosSpectrum1D']['Front'][tof_][scale_] = \
                        self.data['PosSpectrum'][tof_][scale_].get_front_distribution()
                    self.data['PosSpectrum1D']['Back'][tof_][scale_] = \
                        self.data['PosSpectrum'][tof_][scale_].get_back_distribution()
              
            # dlt_E counters + rotation counter
            try:
                self.data['Tof_counters']['tofD1'] += new_data['Tof_counters']['tofD1']
            except sp.WrongSpectrum:
                pass
            
            try:
                self.data['Tof_counters']['tofD2'] += new_data['Tof_counters']['tofD2']
            except sp.WrongSpectrum:
                pass
            
            self.data['Rotation_time'] += new_data['Rotation_time']
            
            # counts, time in seconds
            self.data['counts'] += new_data['current_counts']
            self.data['seconds'] += new_data['current_seconds']
            self.data['current_counts'] = new_data['current_counts']
            self.data['current_seconds'] = new_data['current_seconds']
            # print('counts, sec, cur_c, cur_s: ', 
            #       self.data['counts'], self.data['seconds'],
            #       self.data['current_counts'], self.data['current_seconds'])
            
            # s1 = self.data['Tof_counters']['tofD1'] 
        
        self.GetNewData.emit(True)
         #QtCore.QCoreApplication.postEvent(parent.visualize_manager, PlotUpdateEvent(data))
       
    def get_data_dt(self, new_data):
        
        if new_data is None:
            return
        
        self.data_dt = new_data
        self.GetNewDataTable.emit(True)
         #QtCore.QCoreApplication.postEvent(parent.visualize_manager, PlotUpdateEvent(data))
         
    def clear(self):
        self.data = None
        self.data_dt = None
        gc.collect()
        
        
### Visualization section ---
class VisualizeManager(QtCore.QObject):
    """
    Вся информация о логике переключения окон, слоёв, их взаимодействия
    с кнопками на боковой панели и при нажатии клавиш мыши содержится в
    этом классе.
    
    """
    
    def __init__(self, parent=None):
        QtCore.QObject.__init__(self, parent)
        self.parent = parent
        self.layers = {
            'Main': self.parent.primarywidget, 
            'Box': self.parent.boxwidget, 
            'Single': self.parent.singlewidget
        }
        self.currentLayer = 'Main'
        self.layer_dict = None
        self.parameters = self.init_parameters()
        self.parent.sidewidget.parametertree.setParameters(self.parameters, 
                                                           showTop=False)
        
        self._seconds = 0
        self._counts = 0
        self.init_connections()
        
    def init_parameters(self):
        self.strip_limits_dict = {'Front':(1, 48), 'Back':(1, 128), 
                                  'Side':(1, 64)}
        
        # parameter tree structure
        p = [  
          # spectrum configuration
          {'name': 'Filename', 'type': 'str', 'value': "None", 'readonly':True},
          {'name': 'Detectors', 'type': 'list', 'values': ["Front", "Back", "Side"]},
          {'name': 'Scale', 'type': 'list', 'values': ["alpha", "fission"]},
          {'name': 'dlt_E', 'type': 'list', 'values': [False, True, 'all'], 'value': 'all'},
          {'name': 'x_min', 'type': 'float', 'value': 1000},
          {'name': 'x_max', 'type': 'float', 'value': 10500}, 
          # events counters       
          {'name': 'Events per second', 'type': 'group', 'children': [
              {'name': 'Average', 'type': 'float', 'value': 0., 'readonly': True},
              {'name': 'Current', 'type': 'float', 'value': 0., 'readonly': True},
          ]},    
          {'name': 'IsUpdated', 'type': 'color', 'value': "F00", 'readonly': True},  
          # strip configuration
          {'name': 'Strip', 'type': 'int', 'value': 1, 
                   'limits': (1, 128), 'visible': False}, 
          {'name': 'Strip_min', 'type': 'int', 'value': 1, 
                   'limits': (1, 128), 'visible': False, 'readonly': True}, 
          {'name': 'Strip_max', 'type': 'int', 'value': 16, 
               'limits': (1, 128), 'step': 16, 'visible': False, 'readonly': True}, 
          {'name': 'Single_id', 'type': 'int', 'value': 0, 
               'limits': (0, 11), 'visible': False}, 
          {'name': "Plot summary spectrum", 'type': 'list', 
             'values': [False, True], 'value': False, 'visible': False}
        ]
        parameters = pg.parametertree.Parameter.create(name='parameters', 
            type='group', children=p)
        return parameters        
        
    def Update(self):
        layer = self.currentLayer
        if self.layer_dict is None:
            return
        lr_dict = self.layer_dict[layer]
        parameters = self.layer_dict['parameters_getter'](self)
        widget = self.layers[layer] 
        widget.Clear()
        if layer == 'Main':
            self.plot_Main(lr_dict, parameters, widget)
        elif layer == 'Box':
            self.plot_Box(lr_dict, parameters, widget)
        elif layer == 'Single':
            self.plot_Single(lr_dict, parameters, widget)       
        
#    def customEvent(self, evt):
#        pass
#    def get_max_Yvalue(x_min, x_max, bins, spectrum):
    
    def zoomPlot(self, parameters, widget, bins, data, row=0, col=0, setX=True, setY=True):
        if len(data) < 3:
            return
        x_min, x_max = parameters['x_min'], parameters['x_max']
        id_ = (bins >= x_min) & (bins <= x_max)
        y_max = data[id_[:-1]].max()
        widget.setXRange(x_min, x_max) if setX else None
        widget.setYRange(0., y_max) if setY else None
       
    def plot_Main(self, lr_dict, parameters, widget):
        print('plot main', widget, type(widget))
        for plot_num in lr_dict['Plot']:
            spectrum_getter = lr_dict['Plot'][plot_num]['Spectrum_getter']
            
            if spectrum_getter is None:
                continue
            
            row = (plot_num - 1) // 2
            col = (plot_num - 1) % 2
            widget.setTitle(row, col, widget.Titleslist[row][col])
               
            try:
                spectrum = spectrum_getter(self, **parameters)
            except TypeError as e:
                print(f"EXCEPTION: {e}")
                continue
            
            if plot_num == 9:
                print(f"CHECK dE1-dE2 widget params: {parameters}; \nspectrum: {spectrum}")
            
            if spectrum is None:
                widget_ = widget.plotslist[row][col]
                widget_.Clear()
            try:
                # main spectrum - alpha fission front back side
                if plot_num <= 6:
                    bins, data = spectrum.bins, spectrum.data.sum(axis=1)
                    widget.setData(row, col, bins, data)
                    widget_ = widget.plotslist[row][col]
                    self.zoomPlot(parameters, widget_, bins, data, 
                                  row=row, col=col)
                    self.zoomPlot(parameters, widget_, bins, data, 
                                  row=row, col=col)
                # dE1 - dE2
                elif plot_num == 9:
                    spectrum_tof1, spectrum_tof2 = spectrum
                    widget.setData(row, col, 
                        spectrum_tof1.bins, spectrum_tof1.data,
                        name='tofD1'
                    )
                    widget.setData(row, col, 
                        spectrum_tof2.bins, spectrum_tof2.data,
                        name='tofD2'
                    )
                    widget_ = widget.plotslist[row][col]
                    widget_.setXRange(0, 100)
                # Intensivity timeserie and chains marks
                elif plot_num == 11:
                    
                    if spectrum is None:
                        continue
                    
                    widget_ = widget.plotslist[row][col]
                    
                    # plot intensivity timeserie
                    data = spectrum['Intensivity']
                    x, y = data.bins, data.data
                    widget_.plot_timeserie(x, y)
                    
                    # plot chains
                    data = spectrum['Chains']
                    if data is None:
                        continue
                    
                    print("CHECK CHAINS: ", data, data.coords, data.report)
                    coords, report = data.coords, data.report
                    widget_.plot_chains(coords)
                    # write chains report to the file
                    filename = os.path.basename(self.parent.reader.filename)
                    with open(filename + "_chain.txt", "w") as f:
                        f.write(report)
                    # print report to the report window textbox
                    self.parent.rep_window.textbox.setText(report)
                elif plot_num in [7, 8, 10]:
                    widget.setData(row, col, spectrum.bins, spectrum.data)
            except AttributeError as e:
                print('Plot Main Error: ', e)
               
        #3D plot pos_spectr.data.to_numpy()*8/float(pos_spectr.data.max().max()
        pos_spectr = lr_dict['Plot3D']['Spectrum_getter'](self, **parameters)
        # global check_a, check_b
        # check_a = pos_spectr
        # check_b = lr_dict['Plot3D']
        widget.setData3D(pos_spectr)
                                                 
    def plot_Box(self, lr_dict, parameters, widget):
        print('plot box', widget, type(widget))
        strip_range = np.arange(parameters['Strip_min'], 
                                parameters['Strip_max'] + 1)
        spectrum_getter = lr_dict['Spectrum_getter']
        spectrum = spectrum_getter(self, **parameters)
        for strip_ in strip_range:
            row = ((strip_ - 1) % 16) // 4
            col = ((strip_ - 1) % 16) % 4
            bins, data = spectrum.bins, spectrum[strip_]
            widget.setData(row, col, bins, data)
            widget.setTitle(row, col, str(strip_))
            widget_ = widget.plotslist[row][col]
            self.zoomPlot(parameters, widget_, bins, data, row=row, col=col)
                 
    def plot_Single(self, lr_dict, parameters, widget):
        print('plot single: ', widget, type(widget))
        strip = parameters['Strip']
        plot_summary = parameters['Plot summary spectrum']
        spectrum_getter = lr_dict['Spectrum_getter']
        spectrum = spectrum_getter(self, **parameters)
        id_ = self.parameters.child('Single_id').value()
        titleslist = self.parent.primarywidget.Titleslist
        title = titleslist[id_ // 2][id_ % 2]
        if id_ in range(6):
            if plot_summary:
                data = spectrum.data.sum(axis=1)
                widget.setTitle(title)
            else:
                data = spectrum[strip]
                widget.setTitle(title + ' strip %d' % (strip,))
            widget.setData(spectrum.bins, data)
            
        else:
            widget.setData(spectrum.bins, spectrum.data)   
            data = spectrum.data
            widget.setTitle(title)
            parameters['x_min'] = 0
            parameters['x_max'] = max(spectrum.bins)
        self.zoomPlot(parameters, widget, spectrum.bins, data, 
                      row=None, col=None)  

        
    def initialize_layer_dict(self):
        '''
        Функции, которые достают спектры для каждого окна для различных значений Detectors, 
        Tof, Scale для всех трех слоев(виджетов): Main (главное окно с суммарными спектрами),
        Box (окно с 16 графиками для просмотра спектра со многими стрипами),
        Single (одиночный график во весь экран).
        '''
        layer_dict = {'Main': None, 'Box': None, 'Single': None, 
                      'parameters_getter': None}
        #parameters_getter
        parameters_dict = {'Detectors', 'dlt_E', 'Scale', 'Strip',\
                           'Strip_min', 'Strip_max', 
                           'x_min', 'x_max', 'Single_id',
                           'Plot summary spectrum'}
        parameters_getter = lambda self: {i: j[0] for i, j in 
            list(self.parameters.getValues().items()) if i in parameters_dict}
        layer_dict['parameters_getter'] = parameters_getter
        
        #main layer dict. Structure main_layer_dict[plot_num 1..12]['Sectrum_getter'] -> return func(tof)
        dict_template = {'Spectrum_getter': None, } 
        main_layer_dict = {'Plot': 
                              {i: dict(dict_template) for i in range(1, 13)},
                           'Plot3D': None, 
                           'parameters_getter': None}
        #data['Spectrum']['Front'][tof_][scale_]
        main_layer_dict['Plot'][1]['Spectrum_getter'] = lambda self, dlt_E = None, **options: self.parent.reader.data['Spectrum']['Front'][dlt_E]['alpha'  ]
        main_layer_dict['Plot'][2]['Spectrum_getter'] = lambda self, dlt_E = None, **options: self.parent.reader.data['Spectrum']['Front'][dlt_E]['fission']
        main_layer_dict['Plot'][3]['Spectrum_getter'] = lambda self, dlt_E = None, **options: self.parent.reader.data['Spectrum']['Back' ][dlt_E]['alpha'  ]
        main_layer_dict['Plot'][4]['Spectrum_getter'] = lambda self, dlt_E = None, **options: self.parent.reader.data['Spectrum']['Back' ][dlt_E]['fission']
        main_layer_dict['Plot'][5]['Spectrum_getter'] = lambda self, dlt_E = None, **options: self.parent.reader.data['Spectrum']['Side' ][dlt_E]['alpha'  ]
        main_layer_dict['Plot'][6]['Spectrum_getter'] = lambda self, dlt_E = None, **options: self.parent.reader.data['Spectrum']['Side' ][dlt_E]['fission']
        #self.data['PosSpectrum'][tof_][scale_]
        main_layer_dict['Plot'][7]['Spectrum_getter'] = lambda self, dlt_E = None, Scale = None, **options: self.parent.reader.data['PosSpectrum1D']['Front'][dlt_E][Scale] 
        main_layer_dict['Plot'][8]['Spectrum_getter'] = lambda self, dlt_E = None, Scale = None, **options: self.parent.reader.data['PosSpectrum1D']['Back' ][dlt_E][Scale] 
        #dltE1, dltE2, Tof spectrums
        main_layer_dict['Plot'][9]['Spectrum_getter'] = \
          lambda self, **options: \
            (self.parent.reader.data['Tof_counters']['tofD1'],\
            self.parent.reader.data['Tof_counters']['tofD2'])
        main_layer_dict['Plot'][10]['Spectrum_getter'] = lambda self, **options: self.parent.reader.data['Rotation_time']
        # intensivity timeserie, chains
        main_layer_dict['Plot'][11]['Spectrum_getter'] =  \
          lambda self, **options: self.parent.reader.data_dt 
        #3D plot
        main_layer_dict['Plot3D'] = dict(dict_template)
        main_layer_dict['Plot3D']['Spectrum_getter'] = lambda self, dlt_E = None, Scale = None, **options: self.parent.reader.data['PosSpectrum'][dlt_E][Scale]
        layer_dict['Main'] = main_layer_dict
        
        #box layer
        box_dict = {'Spectrum_getter': None, 'parameters_getter': None}
        box_dict['Spectrum_getter'] = lambda self, Detectors = None, \
         dlt_E = None, Scale = None, **options: \
         self.parent.reader.data['Spectrum'][Detectors][dlt_E][Scale] 
        layer_dict['Box'] = box_dict
        
        #single layer
        single_dict = {'Spectrum_getter': None, 'id_getter': None}
        spectrum_getter = lambda self, Single_id=0, **options: \
          self.layer_dict['Main']['Plot'][Single_id + 1]['Spectrum_getter'](self, **options)
        single_dict['Spectrum_getter'] = spectrum_getter
        def id_getter(self, Detectors=None, Scale=None, **options):
            titleslist = self.parent.primarywidget.Titleslist
            for row_num, row in enumerate(titleslist):
                for col_num, col in enumerate(row):
                    #print 'iterations : ', col, Detectors, Scale, row_num, col_num
                    if (str(Detectors).lower() in col.lower()) & \
                           (str(Scale).lower() in col.lower()):
                        return row_num * len(row) + col_num
        single_dict['id_getter'] = id_getter
        layer_dict['Single'] = single_dict
        
        return layer_dict
        
    def change_layer(self, layer='Main'):
        assert layer in self.layers, 'No such layer!'
        self.currentLayer = layer
        self.parent.stackedbox.setCurrentWidget(self.layers[layer])
        
    # connections
    def init_connections(self):
        # reader
        self.parent.reader.GettingDataSignal.connect(self.on_GettingDataSignal)
        self.parent.reader.GetNewData.connect(self.on_GetNewData)
        self.parent.reader.GetNewDataTable.connect(self.on_GetNewDataTable)
        
        # Primary widget
        plotslist = self.parent.primarywidget.plotslist
        rows, cols = len(plotslist), len(plotslist[0])
        for row_ in range(rows):
            for col_ in range(cols):
                
                # missed widget, space owned by timeserie
                if (row_ == 5) & (col_ == 1):
                    continue
                
                plotslist[row_][col_].DoubleClickedSignal.connect(
                  self.on_PrimaryDoubleClicked)
         
        # Box widget
        plotslist = self.parent.boxwidget.plotslist
        rows, cols = len(plotslist), len(plotslist[0])
        for row_ in range(rows):
            for col_ in range(cols):
                plotslist[row_][col_].DoubleClickedSignal.connect(
                  self.on_BoxDoubleClicked)
                
        # Single widget
        singlewidget = self.parent.singlewidget
        # print('SINGLE: ', singlewidget, singlewidget._plotslist[0].DoubleClickedSignal.connect)
        singlewidget._plotslist[0].DoubleClickedSignal.connect(self.on_SingleDoubleClicked)
#            self.on_SingleDoubleClicked)
        
        # Side widget
        # buttons of ControlPanel
        widget = self.parent.sidewidget
        widget.buttonback.clicked.connect(self.on_buttonbackClicked)
        widget.buttonleft.clicked.connect(self.on_buttonleftClicked)
        widget.buttonright.clicked.connect(self.on_buttonrightClicked)
        widget.buttonleft16.clicked.connect(self.on_buttonleft16Clicked)
        widget.buttonright16.clicked.connect(self.on_buttonright16Clicked)
        
        # parameter Tree
        self.parameters.sigTreeStateChanged.connect(
          self.on_ParameterTreeChanged)
        
    def on_ParameterTreeChanged(self, param, changes):
        print("tree changes:")
        for param, change, data in changes:
            path = self.parameters.childPath(param)
            if path is not None:
                childName = '.'.join(path)
            else:
                childName = param.name()
            print(('  parameter: %s'% childName))
            print(('  change:    %s'% change))
            #print('  data:      %s'% str(data))
            print('  ----------')
        #
        layer = self.currentLayer
        print('currentLayer: ', layer)
        if layer == 'Single':
            lr_dict = self.layer_dict[layer]
            parameters = self.layer_dict['parameters_getter'](self)
            single_id = lr_dict['id_getter'](self, **parameters)
            print('single_id: ', single_id)
            self.parameters.child('Single_id').setValue(single_id)
        self.Update()
        
    def on_SingleDoubleClicked(self, id_):  
        # plot Box widget with plots
        id_ = self.parameters.child('Single_id').value()
        if id_ in range(6):
            self.change_layer('Box')
            titleslist = self.parent.primarywidget.Titleslist
            title = titleslist[id_ // 2][id_ % 2] 
            self.parameters.child('Strip_min').setValue(1)
            self.parameters.child('Strip_max').setValue(16)
            if 'Front' in title:
                self.parameters.child('Detectors').setValue('Front')
            elif 'Back' in title:
                self.parameters.child('Detectors').setValue('Back')
            elif 'Side' in title:
                # self.parameters.child('Strip_min').setValue(1)
                # self.parameters.child('Strip_max').setValue(6)
                self.parameters.child('Detectors').setValue('Side') 
            
            if 'Alpha' in title:
                self.parameters.child('Scale').setValue('alpha')  
            elif 'Fission' in title:
                self.parameters.child('Scale').setValue('fission')
        self.Update()
                
    def on_BoxDoubleClicked(self, id_): 
        self.parameters.child('Plot summary spectrum').setValue(False)
        self.change_layer('Single')  
        strip_min = self.parameters.child('Strip_min').value()
        self.parameters.child('Strip').setValue(id_ + strip_min)
        self.Update()
                
    def on_PrimaryDoubleClicked(self, id_):
        '''Primary widget structure:
        id  list                Name
        0   Titleslist[0][0] = 'Front Alpha'
        1   Titleslist[0][1] = 'Front Fission'
        2   Titleslist[1][0] = 'Back Alpha'
        3   Titleslist[1][1] = 'Back Fission'
        4   Titleslist[2][0] = 'Side Alpha'
        5   Titleslist[2][1] = 'Side Fission'
        6   Titleslist[3][0] = 'Front position'
        7   Titleslist[3][1] = 'Back position'
        8   Titleslist[4][0] = 'dE1 - dE2'
        9   Titleslist[4][1] = 'Rotation Time'
        10  Titleslist[5][0] = 'Intensivity timeserie'
        11  Titleslist[5][1] = ''

        '''
        self.parameters.child('Single_id').setValue(id_)
        self.parameters.child('Plot summary spectrum').setValue(True)
#        if id_ in range(6):
#            self.change_layer('Box')
#            titleslist = self.parent.primarywidget.Titleslist
#            title = titleslist[id_ // 2][id_ % 2] 
#            self.parameters.child('Strip_min').setValue(1)
#            self.parameters.child('Strip_max').setValue(16)
#            if 'Front' in title:
#                self.parameters.child('Detectors').setValue('Front')
#            elif 'Back' in title:
#                self.parameters.child('Detectors').setValue('Back')
#            elif 'Side' in title:
#                self.parameters.child('Strip_min').setValue(1)
#                self.parameters.child('Strip_max').setValue(6)
#                self.parameters.child('Detectors').setValue('Side') 
#            
#            if 'Alpha' in title:
#                self.parameters.child('Scale').setValue('alpha')  
#            elif 'Fission' in title:
#                self.parameters.child('Scale').setValue('fission')
#        elif id_ in range(6, 11):
        self.change_layer('Single')
        self.Update()
        self.Update()
          
    def get_counts_intensivity(self):
        seconds = self.parent.reader.data['seconds']
        counts = self.parent.reader.data['counts']
        seconds_new = self.parent.reader.data['current_seconds']
        counts_new = self.parent.reader.data['current_counts']
        
        average_intensivity = counts / seconds \
            if seconds > 0 else 0
        current_intensivity = counts_new / seconds_new \
            if seconds_new > 0 else 0
                
        return average_intensivity, current_intensivity
    
    def on_GetNewData(self, b):
        average_intensivity, current_intensivity = self.get_counts_intensivity()
         
        self.parameters.child('Events per second', 'Average').setValue(average_intensivity) 
        self.parameters.child('Events per second', 'Current').setValue(current_intensivity) 
        self.parameters.child('IsUpdated').setValue("0F0") #set green flag, that mean the data is updated
        if self.layer_dict is None:
            self.layer_dict = self.initialize_layer_dict()    
        self.Update()
        
    def on_GetNewDataTable(self, b):
        # !TODO update side panel chain message
        self.Update()
        
    def on_GettingDataSignal(self, b):
        self.parameters.child('IsUpdated').setValue("F00") #set red flag, that mean the data is not updated
                
    def on_buttonbackClicked(self):
        self.change_layer('Main')
        self.Update()
        
    def strip_counter(self, summand):
        detectors = self.parameters.child('Detectors').value()
        strip_min, strip_max = self.strip_limits_dict[detectors]
        
        layer = self.currentLayer
        if layer == 'Main':
            return
        elif layer == 'Box':
            # if detectors == 'Side':
            #     return
            current_strip_min = self.parameters.child('Strip_min').value()
            current_strip_max = self.parameters.child('Strip_max').value()
            current_strip_min += summand
            current_strip_max += summand
            if current_strip_max > strip_max:
                current_strip_min = strip_min
                current_strip_max = strip_min + 15
            elif current_strip_min < strip_min:
                current_strip_max = strip_max
                current_strip_min = strip_max - 15
            self.parameters.child('Strip_min').setValue(current_strip_min)
            self.parameters.child('Strip_max').setValue(current_strip_max)
        elif layer == 'Single':
            current_strip = self.parameters.child('Strip').value() + summand
            if current_strip > strip_max:
                current_strip = strip_min
            elif current_strip < strip_min:
                current_strip = strip_max
            self.parameters.child('Strip').setValue(current_strip)   
        self.Update()
    
    def on_buttonleftClicked(self):
        self.parent.boxwidget.setYRange(0, 0, 0, 10)
        self.strip_counter(-1)
    
    def on_buttonrightClicked(self):
        self.strip_counter(1)
    
    def on_buttonleft16Clicked(self):
        self.strip_counter(-16)
    
    def on_buttonright16Clicked(self):
        self.strip_counter(16)   
        
### Main application ---
class MainWindow(QtGui.QMainWindow): 
    
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
       
        self.make_interface()
        self.reader = ReaderClass(self)
        if os.path.isdir('coefs/Jan_2021'):
            self.reader.coefs_folder = 'coefs/Jan_2021'
        self.visualize_manager = VisualizeManager(self)
                    
    def make_interface(self):
       #self.setGeometry(300, 300, 450, 300)
        self.setWindowTitle('DGFRS spectrum viewer')
        widget = QtGui.QWidget()
        self.setCentralWidget(widget)
       
        #set Menu
        self.statusBar().showMessage('ready')
        self.setFocus()
        open_ = QtGui.QAction(QtGui.QIcon('open.png'), 'Open', self)
        open_.setShortcut('Ctrl+O')
        open_.setStatusTip('Open new File')
        # self.connect(open_, QtCore.SIGNAL('triggered()'), 
        #              self.showOpenFileDialog)
        open_.triggered.connect(self.showOpenFileDialog)
        #
        track_ = QtGui.QAction(QtGui.QIcon('open.png'), 'Track folder', self)
        track_.setShortcut('Ctrl+P')
        track_.setStatusTip('Track the chosen folder')
        # self.connect(open_, QtCore.SIGNAL('triggered()'), 
        #              self.showOpenFileDialog)
        track_.triggered.connect(self.showTrackFolderDialog)
        #
        open_ftp_ = QtGui.QAction(QtGui.QIcon('open.png'), 'Open FTP', self)
        open_ftp_.setShortcut('Ctrl+F')
        open_ftp_.setStatusTip('Open FTP')
        # self.connect(open_ftp_, QtCore.SIGNAL('triggered()'), 
        #              self.showOpenFtpDialog)
        open_ftp_.triggered.connect(self.showOpenFtpDialog)
        #
        #
        open_sftp_ = QtGui.QAction(QtGui.QIcon('open.png'), 'Open sFTP', self)
        open_sftp_.setShortcut('Ctrl+S')
        open_sftp_.setStatusTip('Open FTP')
        open_sftp_.triggered.connect(self.showOpenSFtpDialog)
        #
        open_clbr_folder_ = QtGui.QAction(QtGui.QIcon('open.png'), 
                                          'Open calibration folder', self)
        open_clbr_folder_.setShortcut('Ctrl+I')
        open_clbr_folder_.setStatusTip('Open calibration folder')
        # self.connect(open_clbr_folder_, QtCore.SIGNAL('triggered()'), 
        #              self.showOpenClbrFolderDialog)
        open_clbr_folder_.triggered.connect(self.showOpenClbrFolderDialog)
        #
        close_ = QtGui.QAction(QtGui.QIcon('icons/exit.png'),'Exit', self)
        close_.setShortcut('Ctrl+Q')
        close_.setStatusTip('Close app')
        # self.connect(close_, QtCore.SIGNAL('triggered()'), 
        #              QtCore.SLOT('close()'))
        close_.triggered.connect(qApp.quit)
        #
        menubar = self.menuBar()
        file_ = menubar.addMenu('&Menu')
        file_.addAction(open_)
        file_.addAction(track_)
        file_.addAction(open_ftp_)
        file_.addAction(open_sftp_)
        file_.addAction(open_clbr_folder_)
        file_.addAction(close_)
        
        # search menu 
        #  search chains
        search_chains_ = QtGui.QAction(QtGui.QIcon('open.png'), 
            'Search chains', self)
        search_chains_.setStatusTip('Search chains')
        search_chains_.triggered.connect(self.showSearhChainsDialog)
        # add search menu
        search_ = menubar.addMenu('&Search')
        search_.addAction(search_chains_)
       
        #layout structure
        mainbox = QtGui.QHBoxLayout()
        self.stackedbox = QtGui.QStackedLayout()
        sidebox = QtGui.QHBoxLayout()
        widget.setLayout(mainbox)
        mainbox.addLayout(self.stackedbox, stretch=6)
        mainbox.addLayout(sidebox, stretch=1)
        #construct 1st layout - primary set of plots (PrimaryWidget)
        self.primarywidget = PrimaryWidget()
        self.stackedbox.addWidget(self.primarywidget)
        #construct 2nd layout - square 4x4 of plots (BoxWidget)
        self.boxwidget = BoxWidget()
        self.stackedbox.addWidget(self.boxwidget)
        #construct 3rd layout - single plot widget (CrosshairWidget)
        self.singlewidget = SingleWidget()
        self.stackedbox.addWidget(self.singlewidget)
        # sidewidget - navigation and data panel
        self.sidewidget = SideWidget(self)
        sidebox.addWidget(self.sidewidget)
        # general layout composition
        self.resize(1920, 1080)
        
        # add non-modal report window
        self.rep_window = self.add_report_window()
        
    def add_report_window(self):
        h_size = 750
        w_size = 450

        rep_window = QWidget(self, QtCore.Qt.Window)
        rep_window.setWindowTitle("Report window")
        rep_window.resize(w_size, h_size)
        
        rep_window.textbox = QTextEdit(rep_window)
        rep_window.textbox.move(20, 20)
        rep_window.textbox.resize(w_size - 20, h_size - 60)
        
        stackedbox = QtGui.QStackedLayout()
        stackedbox.addWidget(rep_window.textbox)
        rep_window.setLayout(stackedbox)
        
        rep_window.showMinimized()
        return rep_window

    def showOpenFileDialog(self):
        filename, _ = QtGui.QFileDialog.getOpenFileName(self, 'Open file', '.')
        if filename == '':
            return
        self.reader.disableFtpMode()
        self.startDataReading(filename)
        
    def check_filename(self, filename):
        basename = os.path.basename(filename)
        if re.match('[a-zA-Z\+]+\.[0-9]+', basename):
            return True
        return False
    
    def new_files_tracker(self, folder):
        filenames = os.listdir(folder)
        while True:
            new_files = set(os.listdir(folder)) - set(filenames)
            new_files = [s for s in new_files if self.check_filename(s)]
            if new_files:
                filenames.extend(new_files)
                new_files = map(lambda name: os.path.join(folder, name),
                                new_files)
                yield sorted(new_files, 
                             key=lambda file_: os.stat(file_).st_ctime)[-1]               
            else:
                yield None
            
    def showTrackFolderDialog(self):
        """
        Choose folder to automatically start reading each new file 
        that appears there.
        """
        # select folder
        foldername = QtGui.QFileDialog.getExistingDirectory(self, 
          'Track data folder', '.')
        print(cur_time(), ' :: Start tracking folder: ', foldername)
        if foldername == '':
            return
        
        # open latest file 
        current_files = map(lambda name: os.path.join(foldername, name),
                            os.listdir(foldername))
        current_files = [s for s in current_files if self.check_filename(s)]
        filename = get_latest_file(current_files)
        if filename:
            self.startDataReading(filename)
            
        # create tracker generator which is checking all new files in 
        # the folder
        get_new_file = self.new_files_tracker(foldername)
        
        # if process functions finds new files, it starts reading
        def process_new_file():   
            nonlocal get_new_file
            new_file = next(get_new_file)
            if new_file is not None:
                # start new file reading session
                self.startDataReading(new_file)
        
        # create timer and search for new files each 5 seconds
        self.folder_tracker = record()
        gc.collect()
        self.folder_tracker.timer = QtCore.QTimer()
        self.folder_tracker.timer.timeout.connect(process_new_file)
        self.folder_tracker.timer.start(500)
        
       
    def startDataReading(self, filename):
        self.reader.timer.stop()
        self.reader.timer_dt.stop()
        self.reader.clear()
        self.reader.filename = filename
        self.reader.offset = 0
        
        self.visualize_manager.parameters.child('Filename').setValue(
          os.path.basename(filename))
        self.rep_window.textbox.setText("")
        
        # start reading data spectrums thread 
        self.reader.timer.start(15000)
        
        # start reading timeserie and chain searching thread
        self.reader.timer_dt.start(25000)
       
    def showSearhChainsDialog(self):
        sp = self.reader.search_properties
        en_b = self.reader.energy_bundle
        dialog = SearchChainDialog(parent=self, 
                                   search_properties=sp,
                                   energy_bundle=en_b)
        # dialog.open()
        result = dialog.exec()
        if result == QDialog.Accepted:
            sp, en_b = dialog.result
            self.reader.search_properties = sp
            self.reader.energy_bundle = en_b
    
    def showOpenFtpDialog(self):
        ftp_dialog = FtpDialog(parent=self)
        filename = ftp_dialog.local_filePath
        if filename == None:
            return
        self.reader.activateFtpMode(ftp_dialog)
        self.startDataReading(filename)
        
    def showOpenSFtpDialog(self):
        ftp_dialog = sFtpDialog(parent=self)
        filename = ftp_dialog.local_filePath
        if filename == None:
            return
        self.reader.activateFtpMode(ftp_dialog)
        self.startDataReading(filename)
      
    def showOpenClbrFolderDialog(self):
        foldername = QtGui.QFileDialog.getExistingDirectory(self, 
          'Open clbr folder', '.')
        print('folder: ', foldername)
        if foldername == '':
            return
        self.reader.coefs_folder = foldername
        
    def close(self):
        self.reader.ftp_dialog = None
        self.reader.flag = False
        try:
            self.reader.timer_dt.stop()
            self.reader.timer.stop()
        except:
            pass
        self.reader = None
        self.rep_window.close()
        gc.collect()
        super().close()
        
          
if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    sys.stdout = open('log.txt', 'a')
    cd = MainWindow()
    cd.show()
    if sys.flags.interactive != 1 or not hasattr(QtCore, 'PYQT_VERSION'):
        pg.QtGui.QApplication.exec_()
