#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  5 17:15:15 2019

The file contains functions searching for time-position correlated events.
Main format to work with is DATA_EVENT: check data_types.pxd and see 
DESCRIPTION below.

MAIN FUNCTIONS:
    
    1) find_a_a(np.ndarray[DATA_EVENT, cast=True] data_table, \
                        c_bool recoil=False, long dlt_t=20) -  
        
        Find indexes for Recoil-alphas 
        correlations       
    
    2) find_fission(data_table, x, y, time=0) - 
        
        Find all data event which occured after the given time.
        
    3) find_R_SF(np.ndarray[DATA_EVENT, cast=True] data_table, \
                 long dlt_t=20)
        
        Find indexes for Recoil-alphas - self-fission correlations
        
    4) split_by_cells(np.ndarray[DATA_EVENT, cast=True] data_table):
        
        Split data events by cells and build dict:
        defaultdict[(cell_x, cell_y)] -> [int]
        
    5) def find_chains(data_table,
                       energy_bundle, search_properties):
        
        Search time correlated decay chains using search_properties in 
        the cell dict.
    
DESCRIPTION
cdef packed struct EVENT:
    np.int_t event_type # 1 - focal, 2 - back, 3 - side, 4 - veto
    np.int_t strip # 0 .. 128
    np.int_t channel # 1 .. 32000
    np.int_t scale # 0 - alpha, 1 - fission
    np.int64_t time_macro # mcs
    np.int64_t time_micro
    c_bool beam_mark # bool
    c_bool tof # bool
    c_word tofD1
    c_word tofD2
    c_word synchronization_time 
                 

cdef packed struct DATA_EVENT:
    np.int_t event_type # 1 - focal-back, 3 - side
    double energy_front
    double energy_back
    np.int_t cell_x # 0 .. 48
    np.int_t cell_y # 0 .. 128
    np.int64_t time_macro # seconds 
    np.int64_t time_micro # microseconds 
    c_bool beam_mark # bool
    c_bool tof # bool   
    np.uint16 tofD1
    np.uint16 tofD2
    np.uint16 rotation_time
    
"""
import bisect

import numpy as np

from collections import defaultdict

from array_types import event_type, final_event_type

cimport numpy as np

from cython cimport boundscheck, wraparound
from data_types cimport c_bool, c_word, BLOCK, EVENT, c_number, \
                        c_number_array, DATA_EVENT
from libc.math cimport fabs
from cython.parallel cimport prange


class record:
    pass

### EXCEPTIONS
ZERO_INPUT_MSG = 'input data table is empty'

class NoneInputError(Exception):
    pass

#### addition quick functions
@boundscheck(False)
@wraparound(False)
cpdef np.ndarray find_a_a(\
        np.ndarray[DATA_EVENT, cast=True] data_table, \
        c_bool recoil=False, long dlt_t=20):
    '''
    
    Find indexes for Recoil-alphas correlations.
    
    Input:
        data_table (np.ndarray, dtype=DATA_EVENT)
        recoil: flag if first correlated event is recoil event, bool
        dlt_t: time window between two consequent events, mks
        
    Return:
        np.ndarray[:] - array of pairs indexes, 
        where indexes[::2] - recoils, indexes[1::2] - alphas
        
        OR 
        
        None, if no correlations were found
        
    Example:
        indexes = find_a_a(data_table, dlt_t=20000) # front-side pairs in alpha scale.
        initial_events = event_table[indexes[:, 0]] (recoils if recoil=True)
        alphas = event_table[indexes[:, 1]]
        
    '''
    cdef:
        long i, j, index, buffer_size
        long dt_size=len(data_table)
        long[:] pairs_array 
        c_bool tof_mark
   
    assert dt_size > 0, ZERO_INPUT_MSG
    
    buffer_size = dt_size // 10
    pairs_array = np.zeros(buffer_size, dtype=long)
    index = 0
    tof_mark = True
    
    for i in range(dt_size):
        
        if recoil:
            tof_mark = data_table[i].tof
            
        if not tof_mark:
            continue
        
        if data_table[i].event_type == 3: # is side
            continue
        
        for j in range(i + 1, i + 20):
            
            if j >= dt_size - 1:
                break
            
            if (data_table[j].time_micro - data_table[i].time_micro > dlt_t):
                break 
            
            if (data_table[i].cell_x == data_table[j].cell_x) & \
                    (data_table[i].cell_y == data_table[j].cell_y) & \
                    (not data_table[j].tof):
                pairs_array[index] = i
                pairs_array[index + 1] = j
                index += 2
                
                assert index < buffer_size, 'pairs_array is overflow'
    
    assert index > 0, "No correlations were found"
              
    return np.asarray(pairs_array[:index])

@boundscheck(False)
@wraparound(False)   
cpdef np.ndarray find_fission(np.ndarray[DATA_EVENT, cast=True] data_table, 
                   int x, int y, np.int64_t time=0):   

    '''
    find_fission(data_table, x, y, time=0) - find all fission events which 
    occured after the given time.
    return data_table

    Input:
        data_table (np.ndarray, dtype=DATA_EVENT)
        x - front strip 1 .. 48
        y - back strip 1 .. 128
        time: search start time, mks
        
    Return:
        data_table (np.ndarray, dtype=DATA_EVENT) 
        
    Example:
        f_table = find_fission(data_table, 1, 1)
        
    '''
                   
    cdef:
        long i, dt_size=len(data_table)
        np.ndarray[c_bool, cast=True] indexes
    
    assert dt_size > 0, ZERO_INPUT_MSG    
    indexes = np.zeros(dt_size, dtype=np.bool)

    for i in prange(dt_size, nogil=True):
        
        if (data_table[i].time_micro < time):
            continue
        
        if (data_table[i].cell_x == x) & (data_table[i].cell_y == y) & \
                ((data_table[i].energy_front > 40000) |
                 (data_table[i].energy_back > 40000)
                ):
                        
            indexes[i] = True
            
    return np.asarray(data_table[indexes])
 
    
@boundscheck(False)
@wraparound(False)
cpdef np.ndarray find_R_SF(
        np.ndarray[DATA_EVENT, cast=True] data_table, 
        long dlt_t=2000000
    ):
    '''
    
    Find indexes for Recoil-alphas - self-fission correlations
    
    Input:
        data_table (np.ndarray, dtype=DATA_EVENT)
        dlt_t: time window between two consequent events, mks
        
    Return:
        np.ndarray[:] - array of pairs indexes, 
        where indexes[::2] - recoils, indexes[1::2] - alphas
        
    Example:
        indexes = find_a_a(data_table, dlt_t=20000) # front-side pairs in alpha scale.
        initial_events = event_table[indexes[:, 0]] (recoils if recoil=True)
        alphas = event_table[indexes[:, 1]]
                
        OR 
        
        None, if no correlations were found
        
    '''
    cdef:
        long i, j, index, buffer_size
        long dt_size = len(data_table)
        long[:] pairs_array 
        c_bool tof_mark
   
    assert dt_size > 0, ZERO_INPUT_MSG
    
    buffer_size = dt_size // 100
    pairs_array = np.zeros(buffer_size, dtype=long)
    index = 0
    
    for i in range(dt_size):
        
        if data_table[i].energy_front < 30000:
            continue 
        
        tof_mark = data_table[i].tof
            
        if tof_mark:
            continue
        
        if data_table[i].event_type == 3:
            continue
        
        j = i - 1
        while j > 1:
            
            if (data_table[i].time_micro - data_table[j].time_micro > dlt_t):
                break 
            
            if (data_table[i].cell_x == data_table[j].cell_x) & \
                    (data_table[i].cell_y == data_table[j].cell_y) & \
                    (data_table[j].tof):
                pairs_array[index] = j
                pairs_array[index + 1] = i
                index += 2
                
                assert index < buffer_size, 'pairs_array is overflow'
                
            j -= 1
    
    assert dt_size > 0, ZERO_INPUT_MSG 
              
    return np.asarray(pairs_array[:index])

@boundscheck(False)
@wraparound(False)
def split_by_cells(np.ndarray[DATA_EVENT, cast=True] data_table):
    '''
    
    Split data events by cells and build dict:
        defaultdict[(cell_x, cell_y)] -> [int]
        
    Input:
        data_table (np.ndarray, dtype=DATA_EVENT)
        
    Return:
        defaultdict[]
        
    Example:
        dt = next(dt_generator)
        cells_dict = split_by_cells(dt)
        
        # get data table indexes from given cell
        cell_x = 24
        cell_y = 48
        indexes = cells_dict[(cell_x, cell_y)]
        # get data subframe
        dt_new = dt[indexes]
        
    '''
    cdef:
        long i
        long dt_size=len(data_table)
        
    if data_table is None:
        raise NoneInputError
        
    output_dict = defaultdict(list)
        
    assert dt_size > 0, ZERO_INPUT_MSG

    for i in range(dt_size):
            x = data_table[i].cell_x
            y = data_table[i].cell_y
            if (y != 0):
                output_dict[(x, y)].append(i)
                
    return output_dict

def get_sp():
    """Return search_properties template."""
    sp = record()
    sp.time_dlt = 26000000 # microsecons
    sp.chain_length_min = 2
    sp.chain_length_max = 2
    sp.recoil_first = True
    sp.search_fission = False
    return sp

def filter_by_energy(data_table, energies_bundle):
    """
    [DATA_EVENTS], obj -> [DATA_EVENTS]
    
    Filter events from data table according to limits of 
    energies bundle: [(energy1_min, energy1_max, event_type), ...], 
    both ends included.
    
    Energy bundle - list of tuples:
        energy_min, energy_max - energy limits in KeV
        event_type - recoil or alpha event type: 'a': alpha particle, 
        'R': recoil nuclei, 'F': fission fragment, 'S': side only events
        
    Return data_table.
    
    """
    tof_dict = {'a': False, 'R': True}
    ind = np.zeros(len(data_table), dtype=np.bool)
    
    for en_min, en_max, ev_type in energies_bundle:
        
        energy_condition = (data_table['energy_front'] >= en_min) & \
                           (data_table['energy_front'] <= en_max)
                           
        if ev_type == "F":                
            ind = ind | (energy_condition  & \
                         (data_table['event_type'] == 1))
        elif ev_type == "S":
            ind = ind | (energy_condition & \
                         (data_table['event_type'] == 3))
        else:
            ind = ind | (energy_condition &\
                         (data_table['tof'] == tof_dict[ev_type]) & \
                         (data_table['event_type'] == 1))
                
    return data_table[ind]

def check_search_properties(search_properties):
    return all(map(lambda p: hasattr(search_properties, p), 
                   ['time_dlt', 'chain_length_min', 'chain_length_max',
                    'recoil_first', 'search_side', 'search_fission']))
  
@boundscheck(False)
@wraparound(False)
def add_beam_off_events(np.ndarray[DATA_EVENT, cast=True] chain,
                np.ndarray[DATA_EVENT, cast=True] data_table):
    """
    [DATA_EVENT], [DATA_EVENT] -> [DATA_EVENT]
    
    Finds the subsequent beam off events in the data_table 
    and add it to the given chain
    returns new chain array including this events.
    
    """
    cdef:
        long i, dt_size, ch_size
        np.int64_t time_start
        int x, y
        np.ndarray[DATA_EVENT, cast=True] output_dt, bo_events
        np.ndarray[c_bool, cast=True] indexes
        
    # check inputs
    dt_size = len(data_table)
    assert dt_size > 0, ZERO_INPUT_MSG
    
    ch_size = len(chain)
    assert ch_size > 0, ZERO_INPUT_MSG
         
    # get chain x, y, time of the last event in the chain
    x = chain[0].cell_x
    y = chain[0].cell_y
    time_start = chain[ch_size - 1].time_micro
    
    # find beam off events
    indexes = np.zeros(dt_size, dtype=np.bool)
    
    for i in prange(dt_size, nogil=True):
        if (data_table[i].time_micro <= time_start):
            continue
        
        if (data_table[i].cell_x == x) & (data_table[i].cell_y == y) &\
              data_table[i].beam_mark:
            indexes[i] = True
            
    bo_events = data_table[indexes]
    # fissions = find_fission(data_table, x, y, time_start)
    
    if len(bo_events) == 0:
        return chain
        
    # initialize output data table
    output_dt = np.zeros(ch_size + len(bo_events), dtype=final_event_type)
    
    # copy chain data to the new chain
    for i in range(ch_size):
        output_dt[i] = chain[i]
        
    # add beam off events to the chain
    for i in range(len(bo_events)):
        output_dt[ch_size + i] = bo_events[i]
            
    return np.asarray(output_dt)

@boundscheck(False)
@wraparound(False)
def add_fission(np.ndarray[DATA_EVENT, cast=True] chain,
                np.ndarray[DATA_EVENT, cast=True] data_table):
    """
    [DATA_EVENT], [DATA_EVENT] -> [DATA_EVENT]
    
    Finds the subsequent fission event in the data_table 
    and add it to the given chain
    returns new chain array including this events.
    
    """
    cdef:
        long i, dt_size, ch_size
        np.int64_t time_start
        int x, y
        np.ndarray[DATA_EVENT, cast=True] output_dt, fissions
        
    # check inputs
    dt_size = len(data_table)
    assert dt_size > 0, ZERO_INPUT_MSG
    
    ch_size = len(chain)
    assert ch_size > 0, ZERO_INPUT_MSG
         
    # try to find fission event
    x = chain[0].cell_x
    y = chain[0].cell_y
    time_start = chain[ch_size - 1].time_micro
    fissions = find_fission(data_table, x, y, time_start)
    
    if len(fissions) == 0:
        return chain
        
    # initialize output data table
    output_dt = np.zeros(ch_size + 1, dtype=final_event_type)
    
    # copy chain data to the new chain
    for i in range(ch_size):
        output_dt[i] = chain[i]
        
    output_dt[i + 1] = fissions[0]
            
    return np.asarray(output_dt)

@boundscheck(False)
@wraparound(False)
def add_side_events(np.ndarray[DATA_EVENT, cast=True] chain,
                    np.ndarray[DATA_EVENT, cast=True] data_table):
    """
    [DATA_EVENT], [DATA_EVENT] -> [DATA_EVENT]
    
    Finds all side event between the first and the last events
    in the given event chain ([DATA_EVENT] array)
    returns new chain array including this events.
    
    """
    cdef:
        long i, j, k, start, stop, dt_size, out_size, ch_size
        np.int64_t t, t0
        np.ndarray[DATA_EVENT, cast=True] output_dt
        
    # check inputs
    dt_size = len(data_table)
    assert dt_size > 0, ZERO_INPUT_MSG
    
    ch_size = len(chain)
    assert ch_size > 0, ZERO_INPUT_MSG
    
    # calculate time diapason for the chain in the data_table
    t0 = chain[0].time_micro
    start = bisect.bisect(np.asarray(data_table['time_micro']), t0) - 1 # because bisect finds index for one more than need
    
    t0 = chain[ch_size - 1].time_micro
    stop = bisect.bisect(np.asarray(data_table['time_micro']), t0) - 1
    # print("CHECK t_stop and if in msv: ", t0, t0 in np.asarray(data_table['time_micro']))
    # print("CHECK start, stop: ", start, stop)
    # print("CHECK time_micro arr: ", np.asarray(data_table['time_micro']))
    # print("CHECK start-stop: \n", chain, '\n', data_table[start: stop+1])
    
    assert (start < dt_size - 1) & (stop > 0), \
        "add_Recoil: chain does not coincide the data table"
        
    # initialize output data table
    out_size = ch_size * 3
    output_dt = np.zeros(out_size, dtype=final_event_type)
        
    j = 0
    k = 0
    for i in range(start, stop + 1):
        t = data_table[i].time_micro
        
        # add chain events to the new chain
        if t == chain[j].time_micro:
            output_dt[k] = chain[j]
            
            if k == out_size - 1:
                print("Attention! structure is overflowed with chain events")
                break
            
            k += 1
            
            if j < ch_size - 1:
                j += 1
            
        # add side events to the new chain
        if data_table[i].event_type == 3:
            output_dt[k] = data_table[i]
            
            if k == out_size - 1:
                print("structure is overflowed with side events")
                break
            
            k += 1
            
    return np.asarray(output_dt[:k])
            
@boundscheck(False)
@wraparound(False)    
def find_cellchains(\
        np.ndarray[DATA_EVENT, cast=True] data_table, \
        search_properties):
    '''
    
    [DATA_EVENT], obj -> [[DATA_EVENT]]
    
    Search time correlated decay chains using search_properties in cell
    (cell_x, cell_y) or (front strip, back strip). Suppose that
    positions (cell) and energies are already filtered. Each chain contain
    events in descending energies order.
    
    np.ndarray[DATA_EVENT], obj -> [np.ndarray[DATA_EVENT]]
    
    Input:        
    time_dlt : <int> time in microseconds between two consecutive events;
    chain_length_min : <int> minimal length of chains;
    chain_length_max : <int> maximal length of chains; 
                       If has default value -1 -> the upper limit will set
                       to 50;
    recoil_first : <c_bool> check if first event in chain is recoil;
    search_side: <c_bool> check if to include side-only events to the chain
    search_fission : <c_bool> check if search includes fission decay 
                         events (energies > 40000.)
                
    Return: 
        [ndarray[DATA_EVENT]]: list of events chains (ndarrays).
    
    ***    
    Example:
    --------    
    class record:
        pass

    search_properties = record()
    
    #### FIND R-a (recoil-alpha pairs)
    search_properties.time_dlt = 1000
    search_properties.chain_length_min = 2
    search_properties.chain_length_max = 2
    search_properties.recoil_first = True
    search_properties.search_fission = False
    
    chains = find_cellchains(data_table, search_properties)
    print len(chains)
    print chains
        
    '''
    cdef:
        long i, chain_ind, chain_len_min, chain_len_max
        np.int64_t time_dlt
        long dt_size
        c_bool tof_mark, recoil_first, fission_flag
        double energy, energy_pre
        np.ndarray[DATA_EVENT, cast=True] chain
        c_bool time_condition, energy_condition
   
    # check inputs
    dt_size = len(data_table)
    assert dt_size > 0, ZERO_INPUT_MSG
    
    # check search properies record
    assert check_search_properties(search_properties), \
        "search_properties don't have required parameters! See description!"   
    
    chain_len_min = search_properties.chain_length_min
    chain_len_max = search_properties.chain_length_max
    recoil_first = search_properties.recoil_first
    time_dlt = search_properties.time_dlt
    
    if chain_len_max == -1:
        chain_len_max = 50   
    assert chain_len_max >= 2, "chain_len_max should be >= 2"
    assert chain_len_min >= 2, "chain_len_min shoudl be >= 2"
    
    # init values
    chain_ind = 0
    tof_mark = False
    tof_mark_pre = True
    output = []
    
    # main circle - move through data array and collect chains 
    for i in range(1, dt_size):
        
        # if recoil_first is false - collect both alphas and recoils to chains    
        if recoil_first: 
            tof_mark_pre = data_table[i-1].tof
            tof_mark = data_table[i].tof
        
        energy = data_table[i].energy_front
        energy_pre = data_table[i-1].energy_front
        
        # check if timeserie is ordered
        if (data_table[i].time_micro - data_table[i-1].time_micro < 0):
           print "timeserie interrupt indexes: ", i-1, i
           assert False,\
               "Bad dataset - microseconds timeserie isn't ordered!" 
        
        # ascending time, descending energy
        time_condition = (data_table[i].time_micro - \
                          data_table[i-1].time_micro <= time_dlt)
        energy_condition = (energy + 100 < energy_pre)
        
        # recoil energy could be > subsequent alpha event energy
        if recoil_first & (chain_ind == 0): 
            energy_condition = energy_condition | \
                               (tof_mark_pre & (not tof_mark))
                               
        # if search_fission: # add fission decays events to chains
        #     energy_condition = energy_condition | (energy > 40000.)
        
        # construct chain
        if time_condition & energy_condition:
            
            # write event to opened chain 
            if (chain_ind > 0) & (not tof_mark): 
                chain[chain_ind] = data_table[i]
                chain_ind += 1
                
                # close chain if reach max length
                if chain_ind == chain_len_max: 
                    output.append(chain[:chain_ind])
                    chain_ind = 0
             
            # open new chain
            elif tof_mark_pre & (not tof_mark):
                chain = np.zeros(chain_len_max, dtype=final_event_type)
                chain[0] = data_table[i - 1]
                chain[1] = data_table[i]
                chain_ind += 2
                
                # close chain if reach max length
                if chain_ind == chain_len_max: 
                    output.append(chain[:chain_ind])
                    chain_ind = 0
         
        # write chain and close
        elif chain_ind >= chain_len_min: 
            output.append(chain[:chain_ind])
            chain_ind = 0
            
        # close chain    
        else: 
            chain_ind = 0
    
    # add new chain to the chain list (output)
    if chain_ind >= chain_len_min:
        output.append(chain[:chain_ind]) 
        
    return output

def find_chains(data_table, energy_bundle, search_properties):
    '''
    Search time correlated decay chains using search_properties and
    collect them into cell dict.
    
    [DATA_EVENT], [(float, float, str)], obj -> 
    -> defaultdict{[cell_x, cell_y): [np.ndarray[DATA_EVENT]]}
    
    Input: 
        
    data_table (np.ndarray, dtype=DATA_EVENT)
    
    energy_bundle - list of energy limits in KeV, like  [(6620, 6740, 'a'), (9200, 9340, 'a'), ]
       energy_min, energy_max - energy limits in KeV
       event_type - recoil or alpha event type: 'a': alpha particle, 
       'R': recoil nuclei, 'F': fission fragment, 'S': side only events
       
    search_properties - search structure with parameters:
        time_dlt : <int> time in microseconds between two consecutive events;
        chain_length_min : <int> minimal length of chains;
        chain_length_max : <int> maximal length of chains; 
                           If has default value -1 -> the upper limit will set
                           to 50;
        recoil_first : <c_bool> check if first event in chain is recoil;
        search_side: <c_bool> check if to include side-only events to the 
                     chain
        search_fission : <c_bool> check if search includes fission decay 
                         events (energies > 40000.)
                
    Return: cell dict with list of chains
    
    ***    
    Example:
    --------    
    class record:
        pass

    search_properties = record()
    
    #### FIND R-a (recoil-alpha pairs)
    search_properties.time_dlt = 26000000
    search_properties.chain_length_min = 2
    search_properties.chain_length_max = 2
    search_properties.recoil_first = True
    search_properties.search_side = False
    search_properties.search_fission = False
    energy_bundle = [(6620, 6740, 'a'), (9200, 9340, 'a'), ]
        
    dt = next(dt_generator)
    chains = find_chains(dt, energy_bundle, search_properties)
    
    # get data table indexes from given cell
    cell_x = 24
    cell_y = 48
    print chains[(cell_x, cell_y)]
    
    '''
    
    if data_table is None:
        raise NoneInputError("get empty input data as the object of NoneType")
        
    dt_size = len(data_table)
    assert dt_size > 0, ZERO_INPUT_MSG
    
    # check search properies record
    assert check_search_properties(search_properties), \
        "search_properties don't have required parameters! See description!"
        
    # filter by energy
    data_table_old = data_table
    data_table = filter_by_energy(data_table, energy_bundle)
    
    # split data into cells
    cell_dict = split_by_cells(data_table)
    
    # search in each cell and fill output dict
    output = dict()
    
    # iterate over the cells
    for key in cell_dict.keys(): 
        indexes = cell_dict[key]
        current_dt = data_table[indexes]
        
        # find chains in each cell and collect them as list into the dict
        result = find_cellchains(current_dt, search_properties)
        
        if result:
            
            # find & add all side events which coincide the chain
            if search_properties.search_side:
                res_ = []
                for chain in result:
                    res_.append(add_side_events(chain, data_table))
                result = res_
                
            # append all beam off events in the file to the chain
            res_ = []
            for chain in result:
                res_.append(add_beam_off_events(chain, data_table_old))
            result = res_
            
            # try to find susequent fission event and add it to the chain
            if search_properties.search_fission:
                res_ = []
                for chain in result:
                    res_.append(add_fission(chain, data_table))
                result = res_
            output[key] = result
        
    return output
       
cdef void fill_event(EVENT *event, DATA_EVENT d_event, 
        double[:, :, :, ::1] coefficients):
#    Coefs array structure: [event_type 1-front, 2-back, 3-side]
#        [scale 0-alpha, 1-fission][coef 0-a, 1-b][strip 0..129]     
    if d_event.energy_front < 25000.:
        event.scale = 0
    else:
        event.scale = 1
        
    event.time_macro = d_event.time_macro
    event.time_micro = d_event.time_micro
    event.beam_mark = d_event.beam_mark
    event.tof = d_event.tof    
    
    a = coefficients[event.event_type, 0, 0, event.strip]
    b = coefficients[event.event_type, 0, 1, event.strip]
    
    assert a != 0, """Division by zero, %d type in %d strip contains \
        zero coefficients. Time: %d""" % \
        (event.event_type, event.strip, event.time_micro)
    
    event.channel = int((d_event.energy_front - b) / a)
    
def convert_data_event_to_frame_event(
        DATA_EVENT d_event, coefficients):
    cdef:
        EVENT event
        np.ndarray[EVENT, cast=True] event_table
        
    event_table = np.zeros(2, dtype=event_type)
    
    # fill side
    if d_event.event_type == 3:
        event.event_type = d_event.event_type
        event.strip = d_event.cell_x
        fill_event(&event, d_event, coefficients)   
        event_table[0] = event
        return event_table, 1
    
    # fill focal
    if d_event.event_type < 3:
        event.event_type = 1
        event.strip = d_event.cell_x
        fill_event(&event, d_event, coefficients)
        event_table[0] = event
        
        event.event_type = 2
        event.strip = d_event.cell_y  
        fill_event(&event, d_event, coefficients)
        event_table[1] = event
        
        return event_table, 2
    
def get_event(object output):
    cdef:
        DATA_EVENT d_event
        
    for key, value in output.items():
        for chain in value:
            for d_event in chain:
                yield d_event
                
def convert_search_output_to_frame(chains, coefficients):
    """Convert output from find_chains to data frame.
    See data_types.pxd for formats.
    
    """
    
    cdef:
        long frame_len = 0, i = 0, j = 0
        DATA_EVENT d_event
        np.ndarray[DATA_EVENT, cast=True] chain
     
    # calc len of chains frame and build empty data frame
    for key, value in chains.iteritems():
        for item in value:
            frame_len += len(item) * 2            
    event_table = np.zeros(frame_len, dtype=event_type)
    
    # fill the data frame
    for d_event in get_event(chains):
        if d_event.cell_x == 0:
            continue
        events, num = convert_data_event_to_frame_event(d_event, 
            coefficients)
        
        for j in range(num):
            assert i < frame_len, "frame index is out of range"
            event_table[i] = events[j]
            i += 1
                                
    assert i > 0, "frame is empty, no data collected"
    
    return np.asarray(event_table[:i], dtype=event_type)