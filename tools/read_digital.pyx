# -*- coding: utf-8 -*-
"""
Created on Wed Feb 15 10:08:46 2017

@author: eastwood

MAIN FUNCTIONS:
    read_file(filename,strip_convert=True) - read raw digital (american) binary file and return <np.ndarray> event_table (see Description)
"""

from libc.stdio cimport FILE, fopen, fwrite, fscanf, fclose, fprintf, fseek, ftell,SEEK_CUR,SEEK_END, rewind, fread, feof
from libc.stdlib cimport malloc, free                                                           

import struct
import numpy as np
import os

from cython cimport boundscheck,wraparound
cimport numpy as np
cimport cython
from libc.math cimport fabs
from data_types cimport c_bool,c_word,BLOCK,EVENT
from array_types import event_type


#### Description output format (EVENT format)
'''
Output format:
cdef packed struct EVENT:
    np.int_t event_type # 1 - focal, 2 - back, 3 - side, 4 - veto
    np.int_t strip # 0 .. 128
    np.int_t channel # 1 .. 32000
    np.int_t scale # 0 - alpha, 1 - fission
    np.uint64_t time_macro # mcs
    np.uint64_t time_micro
    c_bool beam_mark # bool
    c_bool tof # bool
    c_word tofD1
    c_word tofD2
    c_word synchronization_time 
    
'''

#event_type = np.dtype([('event_type',int),('strip',int),('channel',int),('scale',int),('time',np.double),\
#    ('beam_mark',np.bool),('tof',np.bool),('synchronization_time',np.double),('DAD4',np.uint16),('DAD5',np.uint16),('target_time',np.uint16)])
 
#### Description input format (BLOCK_HEADER + dgEVENT)
'''
    Header_block:
        list of events:
        .
        .
        .
'''
cdef struct BLOCK_HEADER:
    np.int64_t time # seconds time in seconds since the epoch
    np.int64_t time_sync 
    np.int64_t time_inner # inner time counter time 10*nanoseconds
    c_bool beam_on 
    c_bool count_tof_cameras
    np.int32_t count_events 
    
cdef struct dgEVENT:
    np.int32_t position
    np.int32_t amplitude
      
    
cdef BLOCK_HEADER* read_block(BLOCK_HEADER *block_header, FILE* file_):  
    fread(<void*>block_header, sizeof(BLOCK_HEADER), 1, file_)
    return block_header
    
#cdef int add_event_to_data(np.ndarray[DTYPE, cast=True] data, DTYPE event_,int index):
#    data[index] = event_
#    return 1
    

def read_file(filename,offset=0,**argv): #,long chunk_size=10000
    """
    Read events from binary file.
    Each event includes
        	id: 1-front, 2-back, 3-side, 
        	strip: (1,48)-front, (1,128)-back, (1,6)-side 
        	channel - amplitudes
        	time [microseconds] computer clock
           time_sync 
           time_inner [10 x nanoseconds]
        	beam_marker
        	tof
        	veto
    """  
    
    file_size = os.stat(filename).st_size
    sizeof_data = file_size//sizeof(dgEVENT)//3
    cfile = fopen(filename, 'rb')
    fseek(cfile, offset, SEEK_CUR)
    
    cdef:
        #long offset=0
        long long bytes_ = 0
        int count_signals = 0, count_signals_all=0        
        int block_size = 32
        long num_blocks = 0
        long size_msv  
        int pos, chan 
        int count_time_measure = 100  
        int chunk = 0
        long id_=0 #int(chunk_size)  
        long block_start_id_ = 0
        long i = 0
        np.ndarray[EVENT, cast=True] data = np.zeros(sizeof_data,dtype=event_type)
        BLOCK_HEADER *block_header = <BLOCK_HEADER*>malloc(sizeof(BLOCK_HEADER)) 
        dgEVENT *event_ = <dgEVENT*>malloc(sizeof(dgEVENT)) 
        c_bool tof 
        c_bool veto 
        
       
    #reading data block by block  
    while not feof(cfile):#(chunk < chunk_size):
        
        block_header = read_block(block_header,cfile)
        bytes_ += sizeof(BLOCK_HEADER)
        count_events = block_header.count_events
        tof = 0
        veto = 0
        block_start_id_ = id_
        for i in range(count_events):
            fread(event_,sizeof(dgEVENT),1,cfile)
            bytes_ += sizeof(dgEVENT)
        
            if feof(cfile):
                bytes_ -= sizeof(dgEVENT)*count_events + sizeof(BLOCK_HEADER)
                break
            
            try:
                if (event_.position == 121)or(event_.position==122):
                    tof = 1   
                elif event_.position == 112:
                    veto = 1    
                elif event_.position != 0:     
                    
                    data[id_].time_macro = block_header.time
                    data[id_].time_micro = block_header.time_inner#block_header.time_sync
                    data[id_].beam_mark = block_header.beam_on
                    if event_.amplitude > 32000:
                        continue
                    data[id_].channel = event_.amplitude
                    if data[id_].channel > 3000: #according to calibration of 10Dec2017 there're no Th217 peaks (9.21 MeV) higher than 2400 chanell at back strips
                        data[id_].scale = 1
                    else:
                        data[id_].scale = 0    
#                   data[id_].time_inner = block_header.time_inner
                    if (event_.position >0) and(event_.position <=48): #front strips    
                        data[id_].event_type = 1
                        data[id_].strip = event_.position  
                    elif event_.position <0: #back_strips
                        data[id_].event_type = 2
                        data[id_].strip = -event_.position
                        
                        #addition back strip correct - change 1st and 2nd blocks
                        if (data[id_].strip >0)&(data[id_].strip <17):
                            data[id_].strip += 16
                        elif (data[id_].strip >16)&(data[id_].strip <33):
                            data[id_].strip -= 16
                    elif (event_.position >= 103) and (event_.position <= 108): #side detectors
                        data[id_].event_type = 3
                        data[id_].strip = event_.position -102 
                        
                    if veto:
                        data[id_].event_type = 4
                        data[id_].strip = 0
            except IndexError:
                print 'IndexError occured!'
                print id_,sizeof_data
                print 'memory',bytes_,file_size
            if (event_.amplitude > 0) & (event_.amplitude < 32000)&(data[id_].event_type>0):
                id_ += 1
        #set tof and veto flags to data table
        if tof | veto:
            for i in range(block_start_id_,id_):
                data[i].tof = tof
                if veto:
                    data[i].event_type = 4 
    
    fclose(cfile)
    free(block_header)
    free(event_)
    
    print 'Check file reading:', bytes_, file_size, '(the read part of a file, complete file size)'
    return data[:id_],file_size
    
    
def read_probe_file(filename): #,long chunk_size=10000
    """
    Read first 100 events from binary file for probing and print them up.
    Each event includes
        	id: 1-front, 2-back, 3-side, 
        	strip: (1,48)-front, (1,128)-back, (1,6)-side 
        	channel - amplitudes
        	time [microseconds] computer clock
           time_sync 
           time_inner [10 x nanoseconds]
        	beam_marker
        	tof
        	veto
    """  
    
    file_size = os.stat(filename).st_size
    sizeof_data = file_size//sizeof(dgEVENT)
    cfile = fopen(filename, 'rb')
    #fseek(cfile, offset, SEEK_CUR)
    
    cdef:
        #long offset=0
        long long bytes_ = 0
        int count_signals = 0, count_signals_all=0        
        int block_size = 32
        long num_blocks = 0
        long size_msv  
        int pos, chan 
        int count_time_measure = 100  
        int chunk = 0
        long id_=0 #int(chunk_size)  
        long i = 0
        
        np.ndarray[EVENT, cast=True] data = np.zeros(sizeof_data,dtype=event_type)
        BLOCK_HEADER *block_header = <BLOCK_HEADER*>malloc(sizeof(BLOCK_HEADER)) 
        dgEVENT *event_ = <dgEVENT*>malloc(sizeof(dgEVENT)) 
        c_bool tof 
        c_bool veto 
        
       
    #reading data block by block  
    while (id_ < 50) and not feof(cfile):#not feof(cfile):#(chunk < chunk_size):
        
        block_header = read_block(block_header,cfile)
        bytes_ += sizeof(BLOCK_HEADER)
        count_events = block_header.count_events
        tof = 0
        veto = 0
        
        if count_events > 3:
                print '\nHEADER:'
                print 'time',block_header.time
                print 'time_sync',block_header.time_sync
                print 'time_inner',block_header.time_inner
                print 'tof_counter',block_header.count_tof_cameras
                print 'beam',block_header.beam_on
                print 'EVENTS:'
                id_ +=1    
        for i in range(count_events):
            fread(event_,sizeof(dgEVENT),1,cfile)
            bytes_ += sizeof(dgEVENT)
            
            if feof(cfile):
                bytes_ -= sizeof(dgEVENT)*count_events + sizeof(BLOCK_HEADER)
                break
            
        
#    np.int64_t time 
#    np.int64_t time_sync 
#    np.int64_t time_inner 
#    c_bool beam_on 
#    c_bool count_tof_cameras
#    np.int32_t count_events 
#    
#cdef struct dgEVENT:
#    np.int32_t position
#    np.int32_t amplitude
              
            if count_events>3:
                print '     amplitude',event_.amplitude
                print '     pos',event_.position,'\n'
        
#            try:
#                if (event_.position == 121)or(event_.position==122):
#                    tof = 1   
#                    if i > 1:
#                        print 'tof alert!',bytes_
#                elif event_.position == 112:
#                    veto = 1    
#                elif event_.position != 0:     
#                    data[id_].tof = tof
#                    
#                    data[id_].time = block_header.time
#                    data[id_].time_micro = block_header.time_inner#block_header.time_sync
#                    data[id_].beam_mark = block_header.beam_on
#                    data[id_].channel = event_.amplitude
#                    if data[id_].channel > 3000: #according to calibration of 10Dec2017 there're no Th217 peaks (9.21 MeV) higher than 2400 chanell at back strips
#                        data[id_].scale = 1
#                    else:
#                        data[id_].scale = 0    
##                   data[id_].time_inner = block_header.time_inner
#                    if (event_.position >0) and(event_.position <=48): #front strips    
#                        data[id_].event_type = 1
#                        data[id_].strip = event_.position  
#                    elif event_.position <0: #back_strips
#                        data[id_].event_type = 2
#                        data[id_].strip = -event_.position
#                        
#                        #addition back strip correct - change 1st and 2nd blocks
#                        if (data[id_].strip >0)&(data[id_].strip <17):
#                            data[id_].strip += 16
#                        elif (data[id_].strip >16)&(data[id_].strip <33):
#                            data[id_].strip -= 16
#                    elif event_.position >= 103: #side detectors
#                        data[id_].event_type = 3
#                        data[id_].strip = event_.position -102 
#                        
#                    if veto:
#                        data[id_].event_type = 4
#            except IndexError:
#                print id_,sizeof_data
#                print 'memory',bytes_,file_size
#            if event_.amplitude > 0:
#                id_ += 1
    
    fclose(cfile)
    free(block_header)
    free(event_)
    
    print 'Check file reading:', bytes_, file_size, '(the read part of a file, complete file size)'
    #return data[:id_]